module AAM.Util where

import Control.Arrow ((&&&))
import qualified Data.Map as M
import qualified Data.Set as S

-- |
-- Create a lookup `Map` from a list, using a key projection.
--
mkLookup :: Ord k => (v -> k) -> [v] -> M.Map k v
mkLookup getKey = M.fromList . map (getKey &&& id)

-- |
-- Faster `nub` with the minor drawback of requiring an `Ord` instance rather
-- than `Eq`.
--
sortedNub :: Ord k => [k] -> [k]
sortedNub = S.toList . S.fromList
