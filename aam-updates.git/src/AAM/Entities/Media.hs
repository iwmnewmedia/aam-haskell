{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Media where

import AAM.Entities.Common
import Control.Applicative
import Control.DeepSeq
import Control.Parallel.Strategies
import Data.Char (isAlphaNum)
import Data.Csv (FromNamedRecord(..), (.:))
import Data.List (isInfixOf)
import Data.Maybe (mapMaybe)
import qualified Data.Map as M
import qualified Data.Text as T

--------------------------------------------------------------------------------

data Media = Media
  { mId :: ID Media
  , mCaption :: Value MediaCaption
  } deriving (Eq, Ord, Show)

data MediaCaption

--------------------------------------------------------------------------------

instance NFData Media

instance FromNamedRecord Media where
  parseNamedRecord r =
    Media <$> r .: "id"
          <*> r .: "caption"

--------------------------------------------------------------------------------

findLinksInCaption :: (NFData b) => M.Map (Value a) (ID b) -> [Media] -> [(ID Media, ID b)]
findLinksInCaption lookupMap = concat . parMap rdeepseq go
  where
  go (Media mediaId caption) = go' mediaId caption `mapMaybe` M.toList lookupMap
  go' mediaId (Value captionText) (x, xId)
    | T.null (runName x) = Nothing
    | otherwise =
      let splitKey = T.split (not . isAlphaNum) (runName x)
          splitCaption = T.split (not . isAlphaNum) captionText
      in if splitKey `isInfixOf` splitCaption
         then Just (mediaId, xId)
         else Nothing
