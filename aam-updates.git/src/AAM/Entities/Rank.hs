{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Rank where

import AAM.Entities.Common
import Control.Applicative
import Control.Arrow ((&&&))
import Control.DeepSeq
import Data.Csv (FromNamedRecord(..), (.:))
import Data.Maybe (fromMaybe)
import qualified Data.Map as M

--------------------------------------------------------------------------------

data Rank = Rank
  { rId :: ID Rank
  , rName :: Value Rank
  } deriving (Eq, Ord, Show)

data RankAlias = RankAlias
  { raMain :: Value Rank
  , raAlias :: Value Rank
  } deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

instance NFData Rank

instance FromNamedRecord Rank where
  parseNamedRecord r =
    Rank <$> r .: "id"
         <*> r .: "rank"

instance NFData RankAlias

instance FromNamedRecord RankAlias where
  parseNamedRecord r =
    RankAlias <$> r .: "rank"
              <*> r .: "alias"

--------------------------------------------------------------------------------

mkRankAliasLookup :: M.Map (Value Rank) (ID Rank)
                  -> [RankAlias]
                  -> M.Map (Value Rank) (ID Rank)
mkRankAliasLookup ranksByName = M.fromList . map (raAlias &&& lookupRank)
  where
  lookupRank = fromMaybe (error "Missing rank for alias")
             . (`M.lookup` ranksByName)
             . raMain
