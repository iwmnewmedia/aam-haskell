{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Aircraft where

import AAM.Entities.Common
import Control.Applicative
import Control.DeepSeq
import Data.Csv (FromNamedRecord(..), (.:))

--------------------------------------------------------------------------------

data Aircraft = Aircraft
  { acId :: ID Aircraft
  , acSerial :: Value AircraftSerial
  } deriving (Eq, Ord, Show)

data AircraftSerial

--------------------------------------------------------------------------------

instance NFData Aircraft

instance FromNamedRecord Aircraft where
  parseNamedRecord r =
    Aircraft <$> r .: "id"
             <*> r .: "serial_number"
