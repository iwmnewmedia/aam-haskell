{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Unit where

import AAM.Entities.Common
import Control.Applicative
import Control.DeepSeq
import Data.Csv (FromNamedRecord(..), (.:))

--------------------------------------------------------------------------------

data Unit = Unit
  { uId :: ID Unit
  , uName :: Value Unit
  } deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

instance NFData Unit

instance FromNamedRecord Unit where
  parseNamedRecord r =
    Unit <$> r .: "id"
         <*> r .: "name"
