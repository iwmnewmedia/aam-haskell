{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Place where

import AAM.Entities.Common
import AAM.Entities.Location
import Control.Applicative
import Control.DeepSeq
import Data.Csv (FromNamedRecord(..), (.:))
import qualified Data.Map as M

--------------------------------------------------------------------------------

data Place = Place
  { plId :: ID Place
  , plLocation :: ID Location
  } deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

instance NFData Place

instance FromNamedRecord Place where
  parseNamedRecord r =
    Place <$> r .: "id"
          <*> r .: "location_id"

--------------------------------------------------------------------------------

getNameForPlace :: M.Map (ID Location) Location -> Place -> Value Location
getNameForPlace locationsById = maybe (error "Place is missing location") lName
                              . (`M.lookup` locationsById)
                              . plLocation
