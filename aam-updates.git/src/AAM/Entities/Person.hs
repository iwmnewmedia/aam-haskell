{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Person where

import AAM.Entities.Common
import Control.Applicative
import Control.DeepSeq
import Data.Csv (FromNamedRecord(..), (.:))
import qualified Data.Text as T

--------------------------------------------------------------------------------

data Person = Person
  { pId :: ID Person
  , pFirstName :: Value PersonFirstName
  , pMiddleName :: Value PersonMiddleName
  , pLastName :: Value PersonLastName
  } deriving (Eq, Ord, Show)

data PersonFirstName
data PersonMiddleName
data PersonLastName
data PersonName

--------------------------------------------------------------------------------

instance NFData Person

instance FromNamedRecord Person where
  parseNamedRecord r =
    Person <$> r .: "id"
           <*> r .: "firstname"
           <*> r .: "middlename"
           <*> r .: "surname"

--------------------------------------------------------------------------------

-- |
-- Take a person and produce variations of their name with and without the
-- middle name/initial, and a mapping back to the person ID.
--
mkNameVariations :: Person -> [(Value PersonName, ID Person)]
mkNameVariations (Person pid pFirst (Value "") pLast) =
  [ (Value $ T.unwords [runName pFirst, runName pLast], pid) ]
mkNameVariations (Person pid pFirst pMid pLast) =
  [ (Value $ T.unwords [runName pFirst, runName pMid, runName pLast], pid)
  , (Value $ T.unwords [runName pFirst, runName pLast], pid) ]
