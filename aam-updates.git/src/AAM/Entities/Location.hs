{-# LANGUAGE OverloadedStrings #-}

module AAM.Entities.Location where

import AAM.Entities.Common
import Control.Applicative
import Control.DeepSeq
import Data.Csv (FromNamedRecord(..), (.:))

--------------------------------------------------------------------------------

data Location = Location
  { lId :: ID Location
  , lName :: Value Location
  } deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

instance NFData Location

instance FromNamedRecord Location where
  parseNamedRecord r =
    Location <$> r .: "id"
             <*> r .: "name"
