{-# LANGUAGE OverloadedStrings, GeneralizedNewtypeDeriving #-}

module AAM.Entities.Common where

import Control.Applicative
import Control.DeepSeq
import Data.Csv (FromField(..), ToField(..))
import Data.Text (Text())

--------------------------------------------------------------------------------

newtype ID a = ID { runID :: Int } deriving (Show, Ord, Eq)

instance (NFData a) => NFData (ID a) where
  rnf (ID a) = rnf a

instance FromField (ID a) where
  parseField n = ID <$> parseField n

instance ToField (ID a) where
  toField (ID n) = toField n

--------------------------------------------------------------------------------

newtype Value a = Value { runName :: Text } deriving (Eq, Ord, Show, NFData)

instance FromField (Value a) where
  parseField v = Value <$> parseField v

instance ToField (Value a) where
  toField (Value v) = toField v
