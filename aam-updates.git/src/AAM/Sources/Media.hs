{-# LANGUAGE OverloadedStrings #-}

module AAM.Sources.Media (extractPeople) where

import AAM.Entities
import AAM.Util (sortedNub)
import Control.Applicative
import Control.Category
import Control.Parallel.Strategies
import Data.Char
import Data.Function (on)
import Data.List (find, sortBy)
import Data.Monoid ((<>))
import Prelude hiding ((.))
import qualified Data.Attoparsec.Text as P
import qualified Data.Text as T

type PS = P.Parser

--------------------------------------------------------------------------------

extractPeople :: [(Value Rank, ID Rank)] -> [Media] -> [(ID Media, Value PersonName)]
extractPeople rankList = sortedNub . concat . parMap rdeepseq go
  where
  go :: Media -> [(ID Media, Value PersonName)]
  go media = (\(_, name) -> (mId media, name)) `map` findPeople rankList' (mCaption media)

  rankList' :: [(Value Rank, ID Rank)]
  rankList' = sortBy (flip compare `on` (T.length . runName . fst))
            . concatMap addDot
            . filter (not . T.null . runName . fst)
            $ rankList

  addDot :: (Value Rank, ID Rank) -> [(Value Rank, ID Rank)]
  addDot (Value r, i) = [(Value r, i), (Value $ T.intercalate ". " (T.words r) <> ".", i)]

findPeople :: [(Value Rank, ID Rank)] -> Value MediaCaption -> [(ID Rank, Value PersonName)]
findPeople rankList (Value text) = scanCaption text
  where
  scanCaption :: T.Text -> [(ID Rank, Value PersonName)]
  scanCaption = flip go []
    where
    go :: T.Text -> [(ID Rank, Value PersonName)] -> [(ID Rank, Value PersonName)]
    go s result | T.null s = result
    go s result = case find ((`T.isPrefixOf` s) . runName . fst) rankList of
      Nothing -> go (T.drop 1 s) result
      Just (Value x, i) ->
        let s' = T.drop (T.length x) s
        in case P.parseOnly parseName s' of
          Left _ -> go s' result
          Right name -> go s' $ (i, name) : result

    parseName :: PS (Value PersonName)
    parseName = Value . T.unwords . map T.pack <$> (spaces *> P.many1 (lexeme namePart))

    namePart :: PS String
    namePart = P.try properCase <|> (return <$> upper <* optional (P.satisfy (== '.')) <* P.space)

    upper = P.satisfy isUpper
    lower = P.satisfy isLower

    properCase :: PS String
    properCase = do
      x <- upper
      ys <- P.many1 lower
      return (x : ys)

    spaces :: PS ()
    spaces = P.many1 P.space *> return ()

    lexeme :: PS a -> PS a
    lexeme p = p <* spaces
