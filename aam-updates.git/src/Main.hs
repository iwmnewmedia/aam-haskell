{-# LANGUAGE OverloadedStrings #-}

module Main where

import AAM.Entities
import AAM.Util
import Control.Monad (unless)
import Data.Bitraversable (bitraverse)
import Data.Csv (encode, decodeByName, FromNamedRecord(), ToRecord())
import Data.List ((\\))
import Data.Maybe (mapMaybe)
import Data.Monoid ((<>))
import qualified AAM.Sources.Media as AM
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Lazy.Search as LBS
import qualified Data.Map as M
import qualified Data.Vector as V

-- |
-- Load records from the specified CSV using a `FromNamedRecord` instance to
-- decode them. Using `loadData` will probably require a type annotation as the
-- return is polymorphic, similar to the standard `read`.
--
loadData :: FromNamedRecord a => String -> IO [a]
loadData file = do
  csvData <- LBS.readFile file
  case decodeByName (fixEscaping csvData) of
    Left err -> error err
    Right (_, v) -> return $ V.toList v
  where
  fixEscaping :: LBS.ByteString -> LBS.ByteString
  fixEscaping = LBS.replace "\\\"" ("\"\"" :: LBS.ByteString)


-- |
-- Writes a CSV result file using a `ToRecord` instance to encode the rows.
--
writeData :: ToRecord a => FilePath -> [a] -> IO ()
writeData filename rows = do
  putStrLn $ "Writing " ++ filename ++ "..."
  LBS.writeFile ("output/" ++ filename) (encode rows)
  putStrLn $ "Done! (" ++ show (length rows) ++ " rows)"

-- |
--
--
checkMissing :: Show a => String -> (a -> ID a) -> [a] -> [ID a] -> IO ()
checkMissing what getID fromFile fromMap = do
  let missing = map getID fromFile \\ fromMap
  unless (null missing) $ do
    let missing' = filter ((`elem` missing) . getID) fromFile
    putStrLn $ show (length missing') ++ " duplicate entries found when constructing " ++ what ++ " lookup:"
    mapM_ print missing'

-- |
-- Main app entry point, run the processing.
--
main :: IO ()
main = do

  media <- loadData "data/aam_media.csv"
  aircraft <- loadData "data/aam_aircraft.csv"
  locations <- loadData "data/aam_location.csv"
  places <- loadData "data/aam_place.csv"
  people <- loadData "data/aam_person.csv"
  units <- loadData "data/aam_group.csv"
  ranks <- loadData "data/aam_rank.csv"
  rankAliases <- loadData "data/rank-aliases.csv"

  let aircraftBySerial :: M.Map (Value AircraftSerial) (ID Aircraft)
      aircraftBySerial = M.map acId (mkLookup acSerial aircraft)

  let locationsById :: M.Map (ID Location) Location
      locationsById = mkLookup lId locations

  let placesByName :: M.Map (Value Location) (ID Place)
      placesByName = M.map plId (mkLookup (getNameForPlace locationsById) places)

  let unitsByName :: M.Map (Value Unit) (ID Unit)
      unitsByName = M.map uId (mkLookup uName units)

  let ranksByName :: M.Map (Value Rank) (ID Rank)
      ranksByName = M.map rId (mkLookup rName ranks)

  let rankAliasesByName :: M.Map (Value Rank) (ID Rank)
      rankAliasesByName = mkRankAliasLookup ranksByName rankAliases

  let ranksAndAliasesByName = ranksByName <> rankAliasesByName

  checkMissing "aircraft" acId aircraft (M.elems aircraftBySerial)
  checkMissing "places" plId places (M.elems placesByName)
  checkMissing "units" uId units (M.elems unitsByName)
  checkMissing "ranks" rId ranks (M.elems ranksByName)

  -- We only want to generate output for new adlib items
  let newMedia = filter ((>= 7629) . runID . mId) media

  let mediaPeople :: [(ID Media, Value PersonName)]
      mediaPeople = AM.extractPeople (M.toList ranksAndAliasesByName) newMedia

  let peopleLookup :: M.Map (Value PersonName) (ID Person)
      peopleLookup = M.fromList (concatMap mkNameVariations people)

  let mediaPersonLinks :: [(ID Media, ID Person)]
      mediaPersonLinks = sortedNub $ bitraverse Just (`M.lookup` peopleLookup) `mapMaybe` mediaPeople

  let mediaPlaceLinks :: [(ID Media, ID Place)]
      mediaPlaceLinks = findLinksInCaption placesByName newMedia

  let mediaUnitLinks :: [(ID Media, ID Unit)]
      mediaUnitLinks = findLinksInCaption unitsByName newMedia

  let mediaAircraftLinks :: [(ID Media, ID Aircraft)]
      mediaAircraftLinks = findLinksInCaption aircraftBySerial newMedia

  writeData "media-person.csv" mediaPersonLinks
  writeData "media-place.csv" mediaPlaceLinks
  writeData "media-unit.csv" mediaUnitLinks
  writeData "media-aircraft.csv" mediaAircraftLinks
