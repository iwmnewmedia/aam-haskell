# AAM adlib updates

Initialise sandbox and install dependencies:

```
cabal sandbox init
cabal update
cabal install --only-dependencies
```

Building:

```
cabal build
```

Running (will also build first if required):

```
cabal run
```

Also before running an `output` directory must be created, this is where the result CSV files will be written.
