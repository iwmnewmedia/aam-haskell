# AAM data processing

Initialise sandbox and install dependencies:

```
cabal sandbox init
cabal update
cabal install --only-dependencies
```

Building:

```
cabal build
```

Running (will also build first if required):

```
cabal run
```

Also before running an `output` directory must be created, this is where the result CSV files will be written.

## Data sources

The `data` directory contains a collection of CSV files that contain most of the input data provided by AAM, but additionally there are some hard coded data lists under `src/AAM/Sources`:

- `AircraftManufacturer`
- `AircraftModel`
- `Airforce`
- `Rank`

The values stored in these modules match data that exists in the website database and were either provided by AAM or extracted as canonical information from the provided data sources, which was then approved by AAM.

The ADLIB media data is provided by first running a script on the website that imports the data into the website database, this is then exported as the `aam_media.csv` file for use in the process.

All CSV files should be saved as UTF-8 without BOM. This may have to be done manually if the CSVs are edited in Excel or for the generated `aam_media` CSV. If the files are not saved with this charset reading the files will most likely fail before parsing is even attempted.

## Process

The general process runs as follows:

1. Read data sources and attempt to parse entities.
2. Construct the data model.
   - Attempt to merge entities that appear to be referring to the same thing.
   - Substitute concrete values in entites with IDs where relationships need to be made (for example, `AircraftModel "B-17"` values will be replaced with `ID 5`).
3. Attempt to find aircraft, people, places, and unit references within the ADLIB media items.
4. Write CSV output.

During these steps warnings will be printed to the console when un-parseable fields are found, when there are problems merging conflicting values, or when a lookup for an ID fails. In the case of un-parseable fields, these rows are ignored entirely, in merge conflicts the first value is taken, and lookup failures result in the relationship being dropped from the data.

Due to Haskell's non-strict evaluation, some of the data processing does not take place until the files are being written, so when the program starts printing messages about writing files it is normal for this to take quite a long time, especially for the media-related output files.

The program is written to take advantage of multicore processors. On a 4-core hyperthreaded i7-3770 PC it takes approximately 20 minutes to run the process from start to finish. On a MacBook it can take significantly longer.

## Output

The CSV files written to the `output` directory correspond to tables in the website database. The IDs generated are consistent within the generated data but may be remapped when imported into the website.

There is a separate PHP script that is part of the website that performs the import.
