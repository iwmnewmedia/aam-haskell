module Main where

import Data.Bifunctor
import Data.Bitraversable
import Data.Char (isAlphaNum)
import Data.Either (lefts)
import Data.Function (on)
import Data.List (intercalate, find, sortBy, isInfixOf, partition)
import Data.Maybe (catMaybes, mapMaybe, isJust)
import Data.Text (split, pack, unpack)
import Data.Traversable (traverse)
import Data.Tuple (swap)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import qualified Data.Text as T

import Control.Applicative
import Control.Monad
import Control.Monad.Writer (Writer, runWriter, tell)
import Control.Parallel.Strategies

import System.IO (IOMode(..), withFile, hClose, hSetEncoding, hGetContents, hPutStr, hPutStr, utf8)
import Text.CSV (CSV, Record, parseCSV, printCSV)

import AAM.Context
import AAM.Data.Util
import AAM.Entities
import AAM.Pretty
import AAM.Util
import qualified AAM.Sources.AdlibMedia as AM
import qualified AAM.Sources.AircraftManufacturer as AM
import qualified AAM.Sources.AircraftModel as AM
import qualified AAM.Sources.Airforce as AF
import qualified AAM.Sources.DaveOsborne as DO
import qualified AAM.Sources.GroupDescription as GD
import qualified AAM.Sources.MEAF as MEAF
import qualified AAM.Sources.Missions as MS
import qualified AAM.Sources.PaulAndrews as PA
import qualified AAM.Sources.Pilots8AF as PE
import qualified AAM.Sources.PlacesDatabase as PD
import qualified AAM.Sources.Rank as R
import qualified AAM.Sources.ROH9AF as ROH9
import qualified AAM.Sources.UnitsDatabase as UD

-- CSV handling --------------------------------------------------------------

writeCSV :: FilePath -> CSV -> IO ()
writeCSV file csv = withFile file WriteMode $ \handle -> do
  putStrLn $ "Writing " ++ file ++ "..."
  hSetEncoding handle utf8
  hPutStr handle $ printCSV csv
  hClose handle

loadCSV :: FilePath -> IO [Record]
loadCSV file = withFile file ReadMode $ \handle -> do
  putStrLn $ "Loading " ++ file ++ "..."
  hSetEncoding handle utf8
  csv <- hGetContents handle
  case parseCSV file csv of
    Left err -> error (show err)
    Right rows -> return rows

runCSV :: Context -> (Context -> String -> CSV -> a) -> FilePath -> IO a
runCSV ctx parse path = do
  result <- loadCSV path
  putStrLn $ "Parsing " ++ path
  return $ parse ctx path result

printAndRun :: Context -> (Context -> String -> CSV -> ([String], a)) -> FilePath -> IO a
printAndRun ctx parse path = do
  (errors, result) <- runCSV ctx parse path
  unless (null errors) $ putStrLn $ intercalate "\n" errors
  return result

-- Conflict messages --------------------------------------------------------

printConflicts :: ([String], a) -> IO a
printConflicts (xs, a) = do
  unless (null xs) $ putStrLn $ intercalate "\n" xs
  return a

printConflictsW :: Writer [String] (ParseItem a) -> IO (ParseItem a)
printConflictsW w = do
  let (result@(ParseItem pos _), errors) = runWriter w
  unless (null errors) $ do
    putStrLn $ "Error at " ++ show pos ++ ":"
    putStrLn $ "\t" ++ intercalate "\n\t" errors
  return result

-- Output writing ------------------------------------------------------------

writeMap :: (Pretty a) => M.Map a (ID a) -> Record -> FilePath -> IO ()
writeMap items header file =
  writeCSV file $ ("ID" : header) : sortBy (compare `on` (read :: String -> Int) . head) ((pretty . swap) `map` M.toList items)

type LinkWriter k v = [(R k, R v)] -> IO ()

writeLinks :: (Pretty a, Pretty b, Show a, Show b) => FilePath -> Record -> LinkWriter a b
writeLinks file header items =
  writeCSV file $ header : pretty `map` items

------------------------------------------------------------------------------

updatePlace :: M.Map Place (ID Place) -> Either Place (ID Place) -> Either String (Either Place (ID Place))
updatePlace places (Left p) = case find (byNameOrAlt . fst) (M.toList places) of
  Just (_, xid) -> Right $ Right xid
  Nothing -> Left $ "Place lookup failed for " ++ show p
  where
  byNameOrAlt :: Place -> Bool
  byNameOrAlt x = plName p == plName x || plName p `S.member` plAltName x
updatePlace _ other = Right other

updatePlaceByNumber :: M.Map Place (ID Place) -> Either Place (ID Place) -> Either String (Either Place (ID Place))
updatePlaceByNumber places (Left p) = case find (byNumber . fst) (M.toList places) of
  Just (_, xid) -> Right $ Right xid
  Nothing -> Left $ "Place lookup failed for " ++ show p
  where
  byNumber :: Place -> Bool
  byNumber x = plNumber p == plNumber x
updatePlaceByNumber _ other = Right other

findMediaPlaceLinksByLookup :: M.Map PlaceName (ID Place) -> [(AdlibMedia, ID AdlibMedia)] -> [(ID AdlibMedia, ID Place)]
findMediaPlaceLinksByLookup placesByName = catMaybes . parMap rdeepseq go
  where
  go :: (AdlibMedia, ID AdlibMedia) -> Maybe (ID AdlibMedia, ID Place)
  go (AdlibMedia _ (Just (PlaceName p)) _, mediaId) =
        case split (== ',') (pack p) of
          x:_ -> (,) mediaId <$> PlaceName (unpack x) `M.lookup` placesByName
          _ -> Nothing
  go _ = Nothing

findLinksByCaption :: (NFData b) => M.Map a (ID b) -> (a -> String) -> [(AdlibMedia, ID AdlibMedia)] -> [(ID AdlibMedia, ID b)]
findLinksByCaption lookupMap getKey = concat . parMap rdeepseq go
  where
  go (AdlibMedia (Caption c) _ _, mediaId) = go' mediaId c `mapMaybe` M.toList lookupMap
  go' mediaId caption (x, xId)
    | null (getKey x) = Nothing
    | otherwise =
      let splitKey = T.split (not . isAlphaNum) $ T.pack (getKey x)
          splitCaption = T.split (not . isAlphaNum) caption
      in if splitKey `isInfixOf` splitCaption
         then Just (mediaId, xId)
         else Nothing

mkUpdateUnit :: (Show a, Ord a)
             => M.Map UnitName (ID Unit)
             -> (Unit -> a)
             -> M.Map a (ID Unit)
             -> R Unit
             -> WS (Maybe (R Unit))

mkUpdateUnit unitsByName _ _ (Left (Unit Nothing name _ _ _ _ _ _)) =
  case name `M.lookup` unitsByName of
    Nothing -> do
      tell ["Could not match based on name"]
      return Nothing
    Just uId -> return . Just $ Right uId
mkUpdateUnit _ f unitLookup x = substID f unitLookup x

main :: IO ()
main = do

  -- Ranks -------------------------------------------------------------------

  let ctxWithRank = nullCtx { ranks = rName `M.mapKeys` R.ranks }

  -- Data sources ------------------------------------------------------------

  unitDescriptions <- printAndRun ctxWithRank GD.parse "Data/group-descriptions.csv"
  unitItems <- printAndRun ctxWithRank UD.parse "Data/units-database.csv"
  placeItems <- printAndRun ctxWithRank PD.parse "Data/places-database.csv"

  adlibMedia <- M.fromList . map runParseItem <$> printAndRun ctxWithRank AM.parse "Data/aam_media.csv"
  (missionItems, missionEntryItems) <- unzip <$> printAndRun ctxWithRank MS.parse "Data/missions.csv"
  aircraftItemsDO <- printAndRun ctxWithRank DO.parse "Data/do-fortress-log.csv"
  (aircraftItemsPA, aircraftEventItems, peopleItemsPA, peopleEventItemsPA, unitItemsPA) <- printAndRun ctxWithRank PA.parse "Data/pa-roh.csv"
  (peopleItemsROH9, peopleEventItemsROH9, unitItemsROH9) <- printAndRun ctxWithRank ROH9.parse "Data/9af-roh.csv"
  (peopleItems8AF, peopleEventItems8AF) <- printAndRun ctxWithRank PE.parse "Data/8af-pilots-td.csv"
  (peopleItemsMEAF, peopleEventsMEAF) <- printAndRun ctxWithRank MEAF.parse "Data/meaf.csv"

  let unitsItemsMEAF :: [ParseItem Unit]
      unitsItemsMEAF = sortedNub $ traverse (lefts . S.toList . pUnit) `concatMap` peopleItemsMEAF

  -- Airforces ---------------------------------------------------------------

  let airforces = genIDs AF.knownAirforces
  let updateAirforce = substID airforceKey (makeLookup airforceKey airforces)

  -- Places ------------------------------------------------------------------

  places0 <- (printConflictsW . traverse (updateInPlace updateAirforce)) `mapM` placeItems
  places <- genIDs <$> printConflicts (mergeByKey noMerge plName places0)

  -- Units -------------------------------------------------------------------

  let allUnitItems :: [ParseItem Unit]
      allUnitItems = unitItems ++ unitItemsPA ++ unitItemsROH9 ++ unitDescriptions ++ unitsItemsMEAF

      allSubUnitItems :: [ParseItem Unit]
      allSubUnitItems = traverse (lefts . S.toList . uUnit) `concatMap` allUnitItems

  allUnitItems' <- mapM
                   (printConflictsW . traverse (updateInUnit (return . Just) updateAirforce (liftWS $ updatePlace places)))
                   (allUnitItems ++ allSubUnitItems)

  unitsWithID <- genIDs . tidyUpUnitSources <$> printConflicts (mergeByKey mergeUnit unitKey allUnitItems')

  let units :: M.Map Unit (ID Unit)
      units = substInRelationships `M.mapKeys` unitsWithID
        where
        unitsByName = M.fromList $ first uName `map` ((isJust . uType . fst) `filter` M.toList unitsWithID)
        updateUnit :: R Unit -> WS (Maybe (R Unit))
        updateUnit = mkUpdateUnit unitsByName unitKey (makeLookup unitKey unitsWithID)
        substInRelationships :: Unit -> Unit
        substInRelationships u =
          let (result, errors) = runWriter $ updateInUnit updateUnit (return . Just) (return . Just) u
          in if not (null errors) then error "Failure in substInRelationships" else result

  let numMissingUnits = M.size unitsWithID - M.size units
  when (numMissingUnits > 0) $ putStrLn (show numMissingUnits ++ " unit(s) went missing")

  let unitsGroupedByName :: [[(Unit, ID Unit)]]
      unitsGroupedByName = groupSortedBy (uName . fst) (M.toList units)

      units' :: M.Map Unit (ID Unit)
      units' = M.fromList $ go `concatMap` unitsGroupedByName
        where
        go :: [(Unit, ID Unit)] -> [(Unit, ID Unit)]
        go [x] = [x]
        go xs = (isJust . uType . fst) `filter` xs

      unitsByName :: M.Map UnitName (ID Unit)
      unitsByName = M.fromList $ first uName `map` M.toList units'

      updateUnit :: R Unit -> WS (Maybe (R Unit))
      updateUnit = mkUpdateUnit unitsByName unitKey (makeLookup unitKey units')

  -- Aircraft ----------------------------------------------------------------

  let aircraftItems :: [ParseItem Aircraft]
      aircraftItems = aircraftItemsPA ++ aircraftItemsDO

  let updateModel :: R AircraftModel -> WS (Maybe (R AircraftModel))
      updateModel = substID id AM.models

      updateInAircraft' :: Aircraft -> WS Aircraft
      updateInAircraft' = updateInAircraft updateModel (substID id AM.manufacturers) updateAirforce updateUnit

  aircraft0 <- (printConflictsW . traverse updateInAircraft') `mapM` aircraftItems
  aircraft <- genIDs <$> printConflicts (mergeByKey mergeAircraft aircraftKey aircraft0)

  let updateAircraft :: R Aircraft -> WS (Maybe (R Aircraft))
      updateAircraft = substID aircraftKey (makeLookup aircraftKey aircraft)
                   <=< bitraverse updateInAircraft' return

  aircraftEvents0 <- (printConflictsW . traverse (updateInEvent updateAircraft)) `mapM` aircraftEventItems
  aircraftEvents <- genIDs <$> printConflicts (mergeByKey noMerge id aircraftEvents0)

  -- People ------------------------------------------------------------------

  let peopleItems :: [ParseItem Person]
      peopleItems = peopleItemsPA ++ peopleItemsROH9 ++ peopleItems8AF ++ peopleItemsMEAF

      peopleEventItems :: [ParseItem (Event Person)]
      peopleEventItems = peopleEventItemsPA ++ peopleEventItemsROH9 ++ peopleEventItems8AF ++ peopleEventsMEAF

      updateInPerson' :: Person -> WS Person
      updateInPerson' = updateInPerson updateUnit updateAirforce updateAircraft (liftWS $ updatePlaceByNumber places)

  people0 <- (printConflictsW . traverse updateInPerson') `mapM` peopleItems
  people1 <- printConflicts (mergeSomeByKey mergePerson (fst . pServiceNum) people0)
  people2 <- printConflicts (mergeSomeByKey mergePerson (snd . pServiceNum) $ ParseItem (Pos "" 0) `map` people1)
  people <- genIDs <$> printConflicts (mergeByKey mergePerson personKey $ ParseItem (Pos "" 0) `map` people2)

  let personSN1Lookup :: M.Map PersonServiceNumber (ID Person)
      personSN1Lookup = M.fromList $ bitraverse (fst . pServiceNum) Just `mapMaybe` M.toList people

      personSN2Lookup :: M.Map PersonServiceNumber (ID Person)
      personSN2Lookup = M.fromList $ bitraverse (snd . pServiceNum) Just `mapMaybe` M.toList people

      personKeyLookup :: M.Map PersonKey (ID Person)
      personKeyLookup = makeLookup personKey people

      updatePerson :: R Person -> WS (Maybe (R Person))
      updatePerson = liftWS go <=< bitraverse updateInPerson' return
        where
        go :: R Person -> Either String (R Person)
        go (Left (Person (Just sn1, _) _ _ _ _ _ _ _ _ _ _ _ _ _)) =
          maybe (Left $ "Lookup failed for (SN1) " ++ show sn1) (Right . Right) $ M.lookup sn1 personSN1Lookup
        go (Left (Person (_, Just sn2) _ _ _ _ _ _ _ _ _ _ _ _ _)) =
          maybe (Left $ "Lookup failed for (SN2) " ++ show sn2) (Right . Right) $ M.lookup sn2 personSN2Lookup
        go p = substID' personKey personKeyLookup p

  peopleEvents0 <- (printConflictsW . traverse (updateInEvent updatePerson)) `mapM` peopleEventItems

  let (nonUniquePeopleEvents0, uniquePeopleEvents0) = go `partition` peopleEvents0
        where
        go (ParseItem _ e) = eDescription e `elem` ["Born", "Died", "Buried"]

  nonUniquePeopleEvents <- printConflicts (mergeByKey mergeEvent eventKey nonUniquePeopleEvents0)
  uniquePeopleEvents <- printConflicts (mergeByKey noMerge id uniquePeopleEvents0)

  let peopleEvents = genIDs (nonUniquePeopleEvents ++ uniquePeopleEvents)

  -- Missions ----------------------------------------------------------------

  missionLocations <- genIDs <$> printConflicts (mergeByKey noMerge id $ extractMulti meTarget missionEntryItems)
  let updateMissionLocation = substID id missionLocations

  missions <- genIDs <$> printConflicts (mergeByKey leftMerge id missionItems)
  let updateMission = substID id (makeLookup id missions)

  let updateInMissionEntry' = updateInMissionEntry updateMission updateMissionLocation updateModel updateUnit
  missionEntries0 <- (printConflictsW . traverse updateInMissionEntry') `mapM` missionEntryItems
  missionEntries <- genIDs <$> printConflicts (mergeByKey noMerge id missionEntries0)

  -- Media relationships -----------------------------------------------------

  let adlibMediaItems = M.toList adlibMedia

  let placesByName = plName `M.mapKeys` places
  let aircraftBySerial = acSerial `M.mapKeys` aircraft

  let mediaPeopleLookup = M.fromList $ concat $ flip map (M.toList people) $ \(p, k) ->
        case pInitial p of
          Just initial -> [ (unwords [ runPersonFirstName $ pFirstName p
                                     , runPersonInitial initial
                                     , runPersonLastName $ pLastName p
                                     ], k)
                          , (unwords [ runPersonFirstName $ pFirstName p
                                     , runPersonLastName $ pLastName p
                                     ], k)
                          ]
          Nothing -> [ (unwords [ runPersonFirstName $ pFirstName p
                                , runPersonLastName $ pLastName p
                                ], k)
                     ]

  let mediaPeople = (\((_, s), i) -> (s, i)) `map`
        AM.extractPeople (first rName `map` M.toList R.ranks) adlibMediaItems

  let mediaPersonLinks = sortedNub $ bitraverse (`M.lookup` mediaPeopleLookup) Just `mapMaybe` mediaPeople

  let mediaPlaceLinks = sortedNub $ findMediaPlaceLinksByLookup placesByName adlibMediaItems
                                 ++ findLinksByCaption placesByName runPlaceName adlibMediaItems

  let mediaUnitLinks = findLinksByCaption unitsByName runUnitName adlibMediaItems
  let mediaAircraftLinks = findLinksByCaption aircraftBySerial runAircraftSerial adlibMediaItems

  -- Output ------------------------------------------------------------------

  writeMap airforces ["Airforce"] "Output/airforce.csv"
  writeMap places placeHeader "Output/place.csv"
  writeMap units' unitHeader "Output/unit.csv"
  writeMap AM.exportModels acModelHeader "Output/aircraft-model.csv"
  writeMap AM.manufacturers acManufacturerHeader "Output/aircraft-manufacturer.csv"
  writeMap aircraft aircraftHeader "Output/aircraft.csv"
  writeMap aircraftEvents ["Event", "Start date", "End date", "Location name", "Aircraft ID"] "Output/aircraft-event.csv"
  writeMap people personHeader "Output/person.csv"
  writeMap peopleEvents ["Event", "Start date", "End date", "Location name", "Person ID"] "Output/person-event.csv"
  writeMap missions missionHeader "Output/mission.csv"
  writeMap missionEntries missionEntryHeader "Output/mission-entry.csv"
  writeMap missionLocations ["Mission location"] "Output/mission-location.csv"

  writeLinks "Output/unit-place.csv" ["Unit ID", "Place ID"] $ extractLinks units uPlace
  writeLinks "Output/unit-airforce.csv" ["Unit ID", "Airforce ID"] $ extractLinks units uAirforce
  writeLinks "Output/unit-unit.csv" ["Unit ID", "Unit ID"] $ removeReciprocals $ extractLinks units uUnit

  writeLinks "Output/aircraft-airforce.csv" ["Aircraft ID", "Airforce ID"] $ extractLinks aircraft acAirforce
  writeLinks "Output/aircraft-unit.csv" ["Aircraft ID", "Unit ID"] $ extractLinks aircraft acUnit

  writeLinks "Output/place-airforce.csv" ["Place ID", "Airforce ID"] $ extractLinks places plAirforce

  writeLinks "Output/person-aircraft.csv" ["Person ID", "Aircraft ID"] $ extractLinks people pAircraft
  writeLinks "Output/person-airforce.csv" ["Person ID", "Airforce ID"] $ extractLinks people pAirforce
  writeLinks "Output/person-unit.csv" ["Person ID", "Unit ID"] $ extractLinks people pUnit

  writeLinks "Output/mission-entry-unit.csv" ["Mission entry ID", "Unit ID"] $ extractLinks missionEntries meUnit
  writeLinks "Output/mission-entry-mission-location.csv" ["Mission entry ID", "Location ID"] $ extractLinks missionEntries meTarget

  writeLinks "Output/media-place.csv" ["Media ID", "Place ID"] $ bimap Right Right `map` mediaPlaceLinks
  writeLinks "Output/media-unit.csv" ["Media ID", "Unit ID"] $ bimap Right Right `map` mediaUnitLinks
  writeLinks "Output/media-aircraft.csv" ["Media ID", "Aircraft ID"] $ bimap Right Right `map` mediaAircraftLinks
  writeLinks "Output/media-person.csv" ["Person ID", "Media ID"] $ bimap Right Right `map` mediaPersonLinks

  putStrLn "All done!"
