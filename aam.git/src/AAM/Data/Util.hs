module AAM.Data.Util where

import Data.Maybe (catMaybes)
import qualified Data.Set as S

import Control.Applicative
import Control.Monad.Writer (Writer, tell)

-- |
-- Attempt to merge two possible values. If both values are present and are not
-- equivalent, the merge fails with `Left`.
--
(<|-|>) :: (Eq a, Show a) => Maybe a -> Maybe a -> Either String (Maybe a)
(Just x) <|-|> Nothing  = Right $ Just x
Nothing  <|-|> (Just y) = Right $ Just y
Nothing  <|-|> Nothing  = Right Nothing
(Just x) <|-|> (Just y) | x == y = Right $ Just x
                        | otherwise = Left $ "Conflicting values:"
                                          ++ "\n\t\t" ++ show x
                                          ++ "\n\t\t" ++ show y

-- |
-- Type synonym for a writer that accumulates a list of errors.
--
type WS = Writer [String]

-- |
-- Lifts a function supporting failures into WS. `Left` values are sent to the
-- writer and returned as `Nothing`, `Right` values become `Just`.
--
liftWS :: (a -> Either String a) -> a -> WS (Maybe a)
liftWS f x = either (\err -> tell [err] *> return Nothing) (return . Just) $ f x

updateM :: (a -> WS (Maybe a)) -> Maybe a -> WS (Maybe a)
updateM = maybe (return Nothing)

updateS :: (Ord a) => (a -> WS (Maybe a)) -> S.Set a -> WS (S.Set a)
updateS f xs = S.fromList . catMaybes <$> mapM f (S.toList xs)
