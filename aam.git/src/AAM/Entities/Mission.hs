module AAM.Entities.Mission where

import Control.Applicative
import Control.DeepSeq
import Data.List (intercalate)
import Data.Maybe (fromMaybe)
import Data.Time.Calendar
import qualified Data.Set as S

import AAM.Data.Util
import AAM.Entities.Aircraft
import AAM.Entities.Common
import AAM.Entities.Unit
import AAM.Pretty

--------------------------------------------------------------------------------

data Mission =
  Mission { miName :: MissionName
          , miDate :: Day
          , miSource :: S.Set SourceMsg
          } deriving (Show, Eq, Ord)

instance NFData Mission where
  rnf m = rnf (miName m) `seq`
          rnf (miDate m) `seq`
          rnf (miSource m) `seq` ()

instance Pretty Mission where
  pretty m = pretty (miName m)
          ++ [show $ miDate m]
          ++ [intercalate " / " (S.toList $ show `S.map` miSource m)]

missionHeader :: [String]
missionHeader = ["Mission name", "Date", "Source"]

--------------------------------------------------------------------------------

newtype MissionName =
  MissionName { runMissionName :: String } deriving (Show, Eq, Ord)

instance NFData MissionName where
  rnf (MissionName s) = rnf s

instance Pretty MissionName where
  pretty (MissionName s) = [s]

--------------------------------------------------------------------------------

data MissionEntry =
  MissionEntry { meMission :: R Mission
               , meTarget :: S.Set (R MissionEntryTarget)
               , meDescription :: Maybe MissionDescription
               , meAircraftModel :: Maybe (R AircraftModel)
               , meUnit :: S.Set (R Unit)
               , meNotes :: Maybe MissionEntryNotes
               , meTonnage :: Maybe String
               , meStats :: [String]
               } deriving (Show, Eq, Ord)

instance NFData MissionEntry where
  rnf me = rnf (meMission me) `seq`
           rnf (meTarget me) `seq`
           rnf (meDescription me) `seq`
           rnf (meAircraftModel me) `seq`
           rnf (meUnit me) `seq`
           rnf (meNotes me) `seq`
           rnf (meTonnage me) `seq`
           rnf (meStats me) `seq` ()

instance Pretty MissionEntry where
  pretty me = pretty (meMission me)
           ++ maybe [""] pretty (meDescription me)
           ++ maybe [""] pretty (meAircraftModel me)
           ++ maybe [""] pretty (meNotes me)
           ++ maybe [""] pretty (meTonnage me)
           ++ meStats me

missionEntryHeader :: [String]
missionEntryHeader =
  [ "Mission"
  , "Description"
  , "Aircraft model"
  , "Notes"
  , "Tonnage dropped"
  , "Number of aircraft Sent"
  , "Number of aircraft Effective"
  , "Number of aircraft Missing In Action"
  , "Number of aircraft Damaged Beyond Repair"
  , "Number of aircraft Damaged"
  , "Number of people Killed In Action"
  , "Number of people Wounded in Action"
  , "Number of people Missing In Action"
  , "Number of people Evaded"
  , "Number of people Prisoners of War"
  , "Number of people Died in Captivity"
  , "Number of people Interned"
  , "Number of people Returned To Duty"
  , "Enemy aircraft claimed as Destroyed by Bomber Command"
  , "Enemy aircraft claimed as Probably Destroyed by Bomber Command"
  , "Enemy aircraft claimed as Damaged by Bomber Command"
  , "Enemy aircraft claimed as Destroyed by Fighter Command"
  , "Enemy aircraft claimed as Probably Destroyed by Fighter Command"
  , "Enemy aircraft claimed as Damaged by Fighter Command"
  ]

updateInMissionEntry :: (R Mission -> WS (Maybe (R Mission)))
                     -> (R MissionEntryTarget -> WS (Maybe (R MissionEntryTarget)))
                     -> (R AircraftModel -> WS (Maybe (R AircraftModel)))
                     -> (R Unit -> WS (Maybe (R Unit)))
                     -> MissionEntry
                     -> WS MissionEntry

updateInMissionEntry updateMission updateTarget updateAircraftType updateUnit (MissionEntry m t d a u n td s) =
  MissionEntry <$> (fromMaybe (error "Failed to updateMission in updateInMissionEntry") <$> updateMission m)
               <*> updateS updateTarget t
               <*> pure d
               <*> updateM updateAircraftType a
               <*> updateS updateUnit u
               <*> pure n
               <*> pure td
               <*> pure s

--------------------------------------------------------------------------------

newtype MissionEntryTarget =
  MissionEntryTarget { runMissionEntryTarget :: String } deriving (Show, Eq, Ord)

instance NFData MissionEntryTarget where
  rnf (MissionEntryTarget s) = rnf s

instance Pretty MissionEntryTarget where
  pretty (MissionEntryTarget s) = [s]
--------------------------------------------------------------------------------

newtype MissionDescription =
  MissionDescription { runMissionDescription :: String } deriving (Show, Eq, Ord)

instance NFData MissionDescription where
  rnf (MissionDescription s) = rnf s

instance Pretty MissionDescription where
  pretty (MissionDescription s) = [s]

--------------------------------------------------------------------------------

newtype MissionEntryNotes =
  MissionEntryNotes { runMissionEntryNotes :: String } deriving (Show, Eq, Ord)

instance NFData MissionEntryNotes where
  rnf (MissionEntryNotes s) = rnf s

instance Pretty MissionEntryNotes where
  pretty (MissionEntryNotes s) = [s]
