module AAM.Entities.AdlibMedia where

import AAM.Entities.Place
import AAM.Pretty
import Data.Text
import Control.DeepSeq

--------------------------------------------------------------------------------

data AdlibMedia = AdlibMedia
  { amCaption :: Caption
  , amPlace :: Maybe PlaceName
  , amAccession :: AccessionNumber
  }
  deriving (Show, Ord, Eq)

instance NFData AdlibMedia where
  rnf am = rnf (amCaption am) `seq`
           rnf (amPlace am) `seq`
           rnf (amAccession am) `seq` ()

instance Pretty AdlibMedia where
  pretty = error "AdlibMedia is ugly"

--------------------------------------------------------------------------------

newtype Caption = Caption Text
  deriving (Show, Ord, Eq)

instance NFData Caption where
  rnf (Caption x) = rnf x

--------------------------------------------------------------------------------

newtype AccessionNumber = AccessionNumber String
  deriving (Show, Ord, Eq)

instance NFData AccessionNumber where
  rnf (AccessionNumber x) = rnf x
