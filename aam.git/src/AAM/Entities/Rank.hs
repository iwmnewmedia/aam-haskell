module AAM.Entities.Rank where

import AAM.Pretty
import Control.DeepSeq

--------------------------------------------------------------------------------

newtype Rank =
  Rank { rName :: String } deriving (Show, Eq, Ord)

instance NFData Rank

instance Pretty Rank where
  pretty (Rank s) = [s]

rankHeader :: [String]
rankHeader = ["Rank"]

--------------------------------------------------------------------------------
