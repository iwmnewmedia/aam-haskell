module AAM.Entities.Place where

import AAM.Data.Util
import AAM.Entities.Airforce
import AAM.Entities.Common
import AAM.Pretty
import Control.DeepSeq
import Data.List (intercalate)
import qualified Data.Set as S

--------------------------------------------------------------------------------

data Place = Place
  { plNumber :: Maybe StationNumber
  , plName :: PlaceName
  , plAltName :: S.Set PlaceName
  , plLocation :: Maybe Location
  , plDescription :: Maybe PlaceDescription
  , plType :: Maybe PlaceType
  , plAirforce :: S.Set (R Airforce)
  , plSource :: S.Set SourceMsg
  } deriving (Show, Eq, Ord)

instance NFData Place where
  rnf p = rnf (plNumber p) `seq`
          rnf (plName p) `seq`
          rnf (plAltName p) `seq`
          rnf (plLocation p) `seq`
          rnf (plDescription p) `seq`
          rnf (plType p) `seq`
          rnf (plAirforce p) `seq`
          rnf (plSource p) `seq` ()

instance Pretty Place where
  pretty p = maybe [""] pretty (plNumber p)
          ++ pretty (plName p)
          ++ [intercalate "; " $ concatMap pretty (S.toList $ plAltName p)]
          ++ maybe ["", ""] pretty (plLocation p)
          ++ maybe [""] pretty (plDescription p)
          ++ [maybe "Other" show (plType p)]
          ++ [intercalate " / " (S.toList $ show `S.map` plSource p)]

placeHeader :: [String]
placeHeader = ["Station number", "Name", "AKA", "Latitude", "Longitude", "Description", "Type", "Source"]

mkPlace :: String -> Place
mkPlace name = Place Nothing (PlaceName name) S.empty Nothing Nothing Nothing S.empty S.empty

updateInPlace :: (R Airforce -> WS (Maybe (R Airforce)))
              -> Place
              -> WS Place
updateInPlace updateAirforce place = do
  plAirforce' <- updateS updateAirforce (plAirforce place)
  return $ place { plAirforce = plAirforce' }

--------------------------------------------------------------------------------

data PlaceType
  = PlaceMilitary
  | PlaceAirfield
  deriving (Eq, Ord)

instance NFData PlaceType

instance Show PlaceType where
  show PlaceMilitary = "Military site"
  show PlaceAirfield = "Airfield"

--------------------------------------------------------------------------------

newtype StationNumber = StationNumber String deriving (Eq, Ord, Show)

instance NFData StationNumber where
  rnf (StationNumber x) = rnf x

instance Pretty StationNumber where
  pretty (StationNumber n) = [n]

--------------------------------------------------------------------------------

newtype PlaceName = PlaceName { runPlaceName :: String } deriving (Eq, Ord, Show)

instance NFData PlaceName where
  rnf (PlaceName x) = rnf x

instance Pretty PlaceName where
  pretty (PlaceName n) = [n]

--------------------------------------------------------------------------------

newtype PlaceDescription = PlaceDescription String deriving (Eq, Ord, Show)

instance NFData PlaceDescription where
  rnf (PlaceDescription x) = rnf x

instance Pretty PlaceDescription where
  pretty (PlaceDescription n) = [n]
