{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module AAM.Entities.Unit where

import AAM.Entities.Airforce
import AAM.Entities.Common
import AAM.Entities.Place
import AAM.Pretty
import AAM.Data.Util
import Control.Applicative
import Control.DeepSeq
import Data.List (intercalate, sort, unfoldr)
import Data.Monoid
import qualified Data.Set as S

--------------------------------------------------------------------------------

data UnitType
  = Command
  | Division
  | Wing
  | Brigade
  | Regiment
  | Group
  | Squadron
  | Headquarters
  | Battalion
  | Company
  | Platoon
  | Section
  | Misc
  | UnitUnit -- apologies
  | MedicalUnit
  | Detachment
  deriving (Ord, Eq)

instance Show UnitType where
  show Command = "Command"
  show Division = "Division"
  show Wing = "Wing"
  show Brigade = "Brigade"
  show Regiment = "Regiment"
  show Group = "Group"
  show Squadron = "Squadron"
  show Headquarters = "Headquarters"
  show Battalion = "Battalion"
  show Company = "Company"
  show Platoon = "Platoon"
  show Section = "Section"
  show Misc = "Misc"
  show UnitUnit = "Unit"
  show MedicalUnit = "Medical Unit"
  show Detachment = "Detachment"

--------------------------------------------------------------------------------

newtype UnitName = UnitName { runUnitName :: String } deriving (Show, Ord, Eq, Monoid)

instance Pretty UnitName where
  pretty (UnitName s) = [s]

--------------------------------------------------------------------------------

newtype UnitDescription = UnitDescription String deriving (Show, Ord, Eq, Monoid)

instance Pretty UnitDescription where
  pretty (UnitDescription s) = [s]

--------------------------------------------------------------------------------

data Unit = Unit
  { uType :: Maybe UnitType
  , uName :: UnitName
  , uUnit :: S.Set (R Unit)
  , uAirforce :: S.Set (R Airforce)
  , uMedia :: S.Set (R Media)
  , uDescription :: Maybe UnitDescription
  , uPlace :: S.Set (R Place)
  , uSource :: S.Set SourceMsg
  } deriving (Show, Ord, Eq)

instance NFData Unit

unitHeader :: [String]
unitHeader = ["Name", "Type", "Description", "Source"]

instance Pretty Unit where
  pretty u = pretty (uName u)
          ++ [maybe "?" show $ uType u]
          ++ maybe [""] pretty (uDescription u)
          ++ [intercalate " / " (S.toList $ show `S.map` uSource u)]

type UnitKey = (Maybe UnitType, UnitName)

unitKey :: Unit -> UnitKey
unitKey (Unit ty name _ _ _ _ _ _) = (ty, name)

updateInUnit :: (R Unit -> WS (Maybe (R Unit)))
             -> (R Airforce -> WS (Maybe (R Airforce)))
             -> (R Place -> WS (Maybe (R Place)))
             -> Unit
             -> WS Unit
updateInUnit updateUnit updateAirforce updatePlace (Unit t n u af m d p s) =
  Unit <$> pure t
       <*> pure n
       <*> updateS updateUnit u
       <*> updateS updateAirforce af
       <*> pure m
       <*> pure d
       <*> updateS updatePlace p
       <*> pure s

mergeUnit :: Unit -> Unit -> Either String Unit
mergeUnit (Unit t1 n u1 a1 m1 d1 p1 s1) (Unit t2 _ u2 a2 m2 d2 p2 s2) =
  Unit <$> (t1 <|-|> t2)
       <*> pure n
       <*> pure (u1 <> u2)
       <*> pure (a1 <> a2)
       <*> pure (m1 <> m2)
       <*> (d1 <|-|> d2)
       <*> pure (p1 <> p2)
       <*> pure (s1 <> s2)

mkUnit :: Maybe UnitType -> String -> R Unit
mkUnit t n = Left $ Unit t (UnitName n) S.empty S.empty S.empty Nothing S.empty S.empty

-- |
-- Takes a list of units and creates releationships left-to-right, so that an
-- input like:
--   [U1([]), U2([]), U3([])]
-- becomes:
--   [U1'([U2', U3']), U2'([U3']), U3'([])]
--
linkUnits :: [Unit] -> [Unit]
linkUnits [] = []
linkUnits xs = meta . sort $ xs
  where
  meta :: [Unit] -> [Unit]
  meta = unfoldr ana . Just . foldl1 cata
  cata :: Unit -> Unit -> Unit
  cata lastU u = u { uUnit = S.insert (Left lastU) (uUnit u) }
  ana :: Maybe Unit -> Maybe (Unit, Maybe Unit)
  ana Nothing = Nothing
  ana (Just u) | S.null (uUnit u) = Just (u, Nothing)
  ana (Just u) = let u' = either id (error "impossible") $ S.elemAt 0 (uUnit u) in Just (u, Just u')

-- |
-- Removes SourceRef values from units, retaining SourceMsgs
--
tidyUpUnitSources :: [Unit] -> [Unit]
tidyUpUnitSources = map go
  where
  go u = u { uSource = isSourceText `S.filter` uSource u }
