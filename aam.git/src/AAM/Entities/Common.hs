module AAM.Entities.Common where

import AAM.Pretty
import AAM.Data.Util
import Control.Applicative
import Control.DeepSeq
import Data.Foldable
import Data.List (isPrefixOf)
import Data.Traversable
import Data.Time.Calendar
import Data.Maybe (fromMaybe)

--------------------------------------------------------------------------------

newtype ID a = ID Int deriving (Show, Ord, Eq)

instance (NFData a) => NFData (ID a) where
  rnf (ID a) = rnf a

instance Pretty (ID a) where
  pretty (ID n) = [show n]

type R a = Either a (ID a)

--------------------------------------------------------------------------------

data Event a = Event
  { ePreferred :: Bool
  , eDescription :: String
  , eStart :: Maybe Day
  , eEnd :: Maybe Day
  , eLocation :: Maybe LocationName
  , eEntity :: R a
  } deriving (Show, Ord, Eq)

instance (NFData a) => NFData (Event a) where
  rnf e = rnf (ePreferred e) `seq`
          rnf (eDescription e) `seq`
          rnf (eStart e) `seq`
          rnf (eEnd e) `seq`
          rnf (eLocation e) `seq` ()

instance (Show a) => Pretty (Event a) where
  pretty (Event _ desc dt1 dt2 loc (Right pid)) =
    [ desc
    , maybe "" show dt1
    , maybe "" show dt2
    ]
    ++ maybe [""] pretty loc
    ++ pretty pid
  pretty (Event _ _ _ _ _ (Left p)) = error $ "Cannot pretty-print event with Left parent: " ++ show p

updateInEvent :: (R a -> WS (Maybe (R a))) ->
                 Event a ->
                 WS (Event a)

updateInEvent updateParent (Event pref s d1 d2 l p) =
  Event pref s d1 d2 l . fromMaybe (error "Failed to update parent in updateInEvent") <$> updateParent p

type EventKey a = (String, R a)

eventKey :: Event a -> EventKey a
eventKey e = (eDescription e, eEntity e)

mergeEvent :: (Show a, Eq a) => Event a -> Event a -> Either String (Event a)
mergeEvent e1 e2 | e1 == e2 = Right e1
mergeEvent (Event True _ Nothing Nothing Nothing _) e2 = Right e2
mergeEvent e1 (Event True _ Nothing Nothing Nothing _) = Right e1
mergeEvent (Event False _ Nothing Nothing Nothing _) e2 = Right e2
mergeEvent e1 (Event False _ Nothing Nothing Nothing _) = Right e1
mergeEvent (Event True desc start1 end1 l1 e)
           (Event True _    start2 end2 l2 _)
           | start1 == start2 && end1 == end2
  = Event <$> pure True
          <*> pure desc
          <*> pure start1
          <*> pure end1
          <*> mergeLocation l1 l2
          <*> pure e
mergeEvent (Event True _ _ _ _ _)
           (Event True _ _ _ _ _)
  = Left "Cannot merge two equivalent preferred events"
mergeEvent e1 _ | ePreferred e1 = Right e1
mergeEvent _ e2 | ePreferred e2 = Right e2
mergeEvent (Event _ desc start1 end1 loc1 ent)
           (Event _ _    start2 end2 loc2 _)
  = Event <$> pure False
          <*> pure desc
          <*> (start1 <|-|> start2)
          <*> (end1 <|-|> end2)
          <*> mergeLocation loc1 loc2
          <*> pure ent

mergeLocation :: Maybe LocationName -> Maybe LocationName -> Either String (Maybe LocationName)
mergeLocation Nothing loc = Right loc
mergeLocation loc Nothing = Right loc
mergeLocation (Just (LocationName loc1)) (Just (LocationName loc2))
  | loc1 `isPrefixOf` loc2 = Right . Just . LocationName $ loc2
  | loc2 `isPrefixOf` loc1 = Right . Just . LocationName $ loc1
mergeLocation loc1 loc2 =
  Left $ "Cannot merge conflicting locations:\n\t\t" ++ show loc1 ++ "\n\t\t" ++ show loc2

--------------------------------------------------------------------------------

newtype Media = Media String deriving (Show, Ord, Eq)

instance NFData Media where
  rnf (Media x) = rnf x

instance Pretty Media where
  pretty (Media s) = [s]

--------------------------------------------------------------------------------

newtype LocationName = LocationName String deriving (Show, Ord, Eq)

instance NFData LocationName where
  rnf (LocationName x) = rnf x

instance Pretty LocationName where
  pretty (LocationName s) = [s]

locationName :: String -> LocationName
locationName "New York" = LocationName "New York State"
locationName name
  | "NY, New York" `isPrefixOf` name || "New York, New York" `isPrefixOf` name = LocationName "New York State"
  | otherwise = LocationName name

--------------------------------------------------------------------------------

data Location = Location Latitude Longitude deriving (Show, Ord, Eq)

instance NFData Location where
  rnf (Location lat lon) = rnf lat `seq` rnf lon `seq` ()

instance Pretty Location where
  pretty (Location lat lon) = pretty lat ++ pretty lon

--------------------------------------------------------------------------------

newtype Latitude = Latitude Double deriving (Show, Ord, Eq)

instance NFData Latitude where
  rnf (Latitude x) = rnf x

instance Pretty Latitude where
  pretty (Latitude n) = [show n]

--------------------------------------------------------------------------------

newtype Longitude = Longitude Double deriving (Show, Ord, Eq)

instance NFData Longitude where
  rnf (Longitude x) = rnf x

instance Pretty Longitude where
  pretty (Longitude n) = [show n]

--------------------------------------------------------------------------------

data Pos = Pos String Int deriving (Ord, Eq)

instance NFData Pos where
  rnf (Pos file n) = rnf file `seq` rnf n `seq` ()

instance Show Pos where
  show (Pos file n) = file ++ ", row " ++ show n

--------------------------------------------------------------------------------

data ParseItem p = ParseItem Pos p deriving (Show, Ord, Eq)

instance (NFData p) => NFData (ParseItem p) where
  rnf (ParseItem pos p) = rnf pos `seq` rnf p `seq` ()

instance Functor ParseItem where
  fmap f (ParseItem r x) = ParseItem r (f x)

instance Foldable ParseItem where
  foldMap f (ParseItem _ x) = f x

instance Traversable ParseItem where
  traverse f (ParseItem r x) = ParseItem r <$> f x

runParseItem :: ParseItem p -> p
runParseItem (ParseItem _ p) = p

--------------------------------------------------------------------------------

data SourceMsg
  = SourceText String
  | SourceRef String
  deriving (Ord, Eq)

instance NFData SourceMsg where
  rnf (SourceText s) = rnf s
  rnf (SourceRef s) = rnf s

instance Show SourceMsg where
  show (SourceText s) = s
  show (SourceRef s) = s

isSourceText :: SourceMsg -> Bool
isSourceText SourceText{} = True
isSourceText _ = False
