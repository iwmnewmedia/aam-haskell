module AAM.Entities.Aircraft where

import AAM.Data.Util
import AAM.Entities.Airforce
import AAM.Entities.Common
import AAM.Entities.Unit
import AAM.Pretty
import Control.Applicative
import Control.DeepSeq
import Data.List (intercalate)
import Data.Monoid
import qualified Data.Set as S

--------------------------------------------------------------------------------

data Aircraft =
  Aircraft { acSerial :: AircraftSerial
           , acModel :: Maybe (R AircraftModel)
           , acBlockNum :: Maybe AircraftBlockNum
           , acManufacturer :: Maybe (R AircraftManufacturer)
           , acDescription :: Maybe AircraftDescription
           , acAirforce :: S.Set (R Airforce)
           , acUnit :: S.Set (R Unit)
           , acSource :: S.Set SourceMsg
           } deriving (Show, Eq, Ord)

instance NFData Aircraft where
  rnf ac = rnf (acSerial ac) `seq`
           rnf (acModel ac) `seq`
           rnf (acBlockNum ac) `seq`
           rnf (acManufacturer ac) `seq`
           rnf (acDescription ac) `seq`
           rnf (acAirforce ac) `seq`
           rnf (acUnit ac) `seq`
           rnf (acSource ac) `seq` ()

instance Pretty Aircraft where
  pretty a = pretty (acSerial a)
          ++ maybe [""] pretty (acModel a)
          ++ maybe [""] pretty (acBlockNum a)
          ++ maybe [""] pretty (acManufacturer a)
          ++ maybe [""] pretty (acDescription a)
          ++ [intercalate " / " (S.toList $ show `S.map` acSource a)]

aircraftHeader :: [String]
aircraftHeader = ["Serial", "Model", "Block num", "Manufacturer", "Description", "Source"]

type AircraftKey = (AircraftSerial, ID AircraftModel)

aircraftKey :: Aircraft -> AircraftKey
aircraftKey (Aircraft serial (Just (Right modelId)) _ _ _ _ _ _) = (serial, modelId)
aircraftKey ac = error $ "Cannot make key for aircraft that has non-ID values: " ++ show ac

mergeAircraft :: Aircraft -> Aircraft -> Either String Aircraft
mergeAircraft (Aircraft s m block1 manu1 desc1 af1 unit1 s1)
              (Aircraft _ _ block2 manu2 desc2 af2 unit2 s2) =
  Aircraft <$> pure s
           <*> pure m
           <*> (block1 <|-|> block2)
           <*> (manu1 <|-|> manu2)
           <*> (desc1 <|-|> desc2)
           <*> pure (af1 <> af2)
           <*> pure (unit1 <> unit2)
           <*> pure (s1 <> s2)

updateInAircraft :: (R AircraftModel -> WS (Maybe (R AircraftModel))) ->
                    (R AircraftManufacturer -> WS (Maybe (R AircraftManufacturer))) ->
                    (R Airforce -> WS (Maybe (R Airforce))) ->
                    (R Unit -> WS (Maybe (R Unit))) ->
                    Aircraft -> WS Aircraft

updateInAircraft substModel substManufacturer substAirforce substUnit (Aircraft s mdl bn man desc af u s') =
  Aircraft <$> pure s
           <*> updateM substModel mdl
           <*> pure bn
           <*> updateM substManufacturer man
           <*> pure desc
           <*> updateS substAirforce af
           <*> updateS substUnit u
           <*> pure s'

--------------------------------------------------------------------------------

newtype AircraftDescription =
  AircraftDescription String deriving (Show, Eq, Ord)

instance NFData AircraftDescription where
  rnf (AircraftDescription x) = rnf x

instance Pretty AircraftDescription where
  pretty (AircraftDescription d) = [d]

--------------------------------------------------------------------------------

newtype AircraftModel =
  AircraftModel String deriving (Show, Eq, Ord)

instance NFData AircraftModel where
  rnf (AircraftModel x) = rnf x

instance Pretty AircraftModel where
  pretty (AircraftModel n) = [n]

acModelHeader :: [String]
acModelHeader = ["Model"]

--------------------------------------------------------------------------------

newtype AircraftManufacturer =
  AircraftManufacturer String deriving (Show, Eq, Ord)

instance NFData AircraftManufacturer where
  rnf (AircraftManufacturer x) = rnf x

instance Pretty AircraftManufacturer where
  pretty (AircraftManufacturer n) = [n]

acManufacturerHeader :: [String]
acManufacturerHeader = ["Manufacturer"]

--------------------------------------------------------------------------------

newtype AircraftSerial = AircraftSerial { runAircraftSerial :: String }
  deriving (Show, Eq, Ord)

instance NFData AircraftSerial where
  rnf (AircraftSerial x) = rnf x

instance Pretty AircraftSerial where
  pretty (AircraftSerial s) = [s]

--------------------------------------------------------------------------------

newtype AircraftBlockNum =
  AircraftBlockNum String deriving (Show, Eq, Ord)

instance NFData AircraftBlockNum where
  rnf (AircraftBlockNum x) = rnf x

instance Pretty AircraftBlockNum where
  pretty (AircraftBlockNum b) = [b]
