module AAM.Entities.Person where

import AAM.Entities.Aircraft
import AAM.Entities.Airforce
import AAM.Entities.Common
import AAM.Entities.Place
import AAM.Entities.Rank
import AAM.Entities.Unit
import AAM.Pretty
import AAM.Data.Util
import Control.Applicative
import Control.DeepSeq
import Data.List (intercalate, isPrefixOf)
import Data.Maybe (catMaybes)
import Data.Monoid
import qualified Data.Set as S

--------------------------------------------------------------------------------

data Person = Person
  { pServiceNum :: (Maybe PersonServiceNumber, Maybe PersonServiceNumber)
  , pRank :: Maybe (R Rank)
  , pFirstName :: PersonFirstName
  , pInitial :: Maybe PersonInitial
  , pLastName :: PersonLastName
  , pNameSuffix :: Maybe PersonNameSuffix
  , pNickname :: Maybe PersonNickname
  , pRole :: S.Set PersonRole
  , pUnit :: S.Set (R Unit)
  , pAirforce :: S.Set (R Airforce)
  , pAircraft :: S.Set (R Aircraft)
  , pBiography :: Maybe PersonBiography
  , pPlace :: S.Set (R Place)
  , pSource :: S.Set SourceMsg
  } deriving (Show, Eq, Ord)

instance NFData Person where
  rnf p = rnf (pServiceNum p) `seq`
          rnf (pRank p) `seq`
          rnf (pFirstName p) `seq`
          rnf (pInitial p) `seq`
          rnf (pLastName p) `seq`
          rnf (pNameSuffix p) `seq`
          rnf (pNickname p) `seq`
          rnf (pRole p) `seq`
          rnf (pUnit p) `seq`
          rnf (pAirforce p) `seq`
          rnf (pAircraft p) `seq`
          rnf (pBiography p) `seq`
          rnf (pPlace p) `seq`
          rnf (pSource p) `seq` ()

instance Pretty Person where
  pretty p = [intercalate ", " $ runPersonServiceNumber `map` catMaybes [fst (pServiceNum p), snd (pServiceNum p)]]
          ++ maybe [""] pretty (pRank p)
          ++ pretty (pFirstName p)
          ++ maybe [""] pretty (pInitial p)
          ++ pretty (pLastName p)
          ++ maybe [""] pretty (pNameSuffix p)
          ++ maybe [""] pretty (pNickname p)
          ++ [intercalate ", " (concat . S.toList $ pretty `S.map` pRole p)]
          ++ maybe [""] pretty (pBiography p)
          ++ [intercalate " / " (S.toList $ show `S.map` pSource p)]

personHeader :: [String]
personHeader = [ "Service number"
               , "Rank"
               , "First name"
               , "Initial"
               , "Last name"
               , "Name suffix"
               , "Nickname"
               , "Role"
               , "Biography"
               , "Source" ]

updateInPerson :: (R Unit -> WS (Maybe (R Unit)))
               -> (R Airforce -> WS (Maybe (R Airforce)))
               -> (R Aircraft -> WS (Maybe (R Aircraft)))
               -> (R Place -> WS (Maybe (R Place)))
               -> Person
               -> WS Person
updateInPerson updateUnit updateAirforce updateAircraft updatePlace p = do
  pUnit' <- updateS updateUnit (pUnit p)
  pAirforce' <- updateS updateAirforce (pAirforce p)
  pAircraft' <- updateS updateAircraft (pAircraft p)
  pPlace' <- updateS updatePlace (pPlace p)
  return $ p { pUnit = pUnit'
             , pAirforce = pAirforce'
             , pAircraft = pAircraft'
             , pPlace = pPlace'
             }

type PersonKey = (PersonFirstName, Maybe PersonInitial, PersonLastName, S.Set (R Unit))

personKey :: Person -> PersonKey
personKey p = (pFirstName p, pInitial p, pLastName p, pUnit p)

mergePerson :: Person -> Person -> Either String Person
mergePerson p1@(Person (snE1, snO1) ran1 fn i ln ns1 nn1 rol1 u1 af1 ac1 b1 pl1 s1)
            p2@(Person (snE2, snO2) ran2 _  _  _ ns2 nn2 rol2 u2 af2 ac2 b2 pl2 s2)
  = rethrow p1 p2 $ Person <$> ((,) <$> (snE1 <|-|> snE2) <*> (snO1 <|-|> snO2))
                           <*> pure (takeBestRank ran1 ran2)
                           <*> pure fn
                           <*> pure i
                           <*> pure ln
                           <*> (normaliseSuffix ns1 <|-|> normaliseSuffix ns2)
                           <*> (nn1 <|-|> nn2)
                           <*> pure (rol1 <> rol2)
                           <*> pure (u1 <> u2)
                           <*> pure (af1 <> af2)
                           <*> pure (ac1 <> ac2)
                           <*> pure (mergeBiography b1 b2)
                           <*> pure (pl1 <> pl2)
                           <*> pure (s1 <> s2)
  where
  rethrow p1' p2' (Left err) = Left $ "Cannot merge people with conflicting fields:\n\t" ++ show p1' ++ "\n\t" ++ show p2' ++ "\n\t\t" ++ err
  rethrow _ _ other = other
  normaliseSuffix (Just (PersonNameSuffix "Jr")) = Just (PersonNameSuffix "Jr.")
  normaliseSuffix other = other
  takeBestRank r1' r2' | r1' < r2' = r1'
                       | otherwise = r2'
  mergeBiography (Just (PersonBiography b1')) (Just (PersonBiography b2')) = Just (PersonBiography $ b1' ++ "\n\n" ++ b2')
  mergeBiography (Just (PersonBiography b1')) Nothing = Just (PersonBiography b1')
  mergeBiography Nothing b2' = b2'

--------------------------------------------------------------------------------

newtype PersonServiceNumber =
  PersonServiceNumber { runPersonServiceNumber :: String } deriving (Show, Eq, Ord)

instance NFData PersonServiceNumber where
  rnf (PersonServiceNumber s) = rnf s

mkServiceNumber :: Maybe String -> (Maybe PersonServiceNumber, Maybe PersonServiceNumber)
mkServiceNumber (Just "0") = (Nothing, Nothing)
mkServiceNumber Nothing = (Nothing, Nothing)
mkServiceNumber (Just sn')
  | "T-"   `isPrefixOf` sn' = (Nothing, Just $ PersonServiceNumber sn')
  | "O-"   `isPrefixOf` sn' = (Just $ PersonServiceNumber $ '0' : tail sn', Nothing)
  | "A000" `isPrefixOf` sn' = (Just $ PersonServiceNumber $ "0-" ++ drop 4 sn', Nothing)
  | "AO-"  `isPrefixOf` sn' = (Just $ PersonServiceNumber $ "0-" ++ drop 3 sn', Nothing)
  | otherwise               = (Just $ PersonServiceNumber sn', Nothing)

--------------------------------------------------------------------------------

newtype PersonFirstName =
  PersonFirstName { runPersonFirstName :: String } deriving (Show, Eq, Ord)

instance NFData PersonFirstName where
  rnf (PersonFirstName s) = rnf s

instance Pretty PersonFirstName where
  pretty (PersonFirstName s) = [s]

--------------------------------------------------------------------------------

newtype PersonInitial =
  PersonInitial { runPersonInitial :: String } deriving (Show, Eq, Ord)

instance NFData PersonInitial where
  rnf (PersonInitial s) = rnf s

instance Pretty PersonInitial where
  pretty (PersonInitial s) = [s]

--------------------------------------------------------------------------------

newtype PersonLastName =
  PersonLastName { runPersonLastName :: String } deriving (Show, Eq, Ord)

instance NFData PersonLastName where
  rnf (PersonLastName s) = rnf s

instance Pretty PersonLastName where
  pretty (PersonLastName s) = [s]

--------------------------------------------------------------------------------

newtype PersonNameSuffix =
  PersonNameSuffix String deriving (Show, Eq, Ord)

instance NFData PersonNameSuffix where
  rnf (PersonNameSuffix s) = rnf s

instance Pretty PersonNameSuffix where
  pretty (PersonNameSuffix s) = [s]

--------------------------------------------------------------------------------

newtype PersonNickname =
  PersonNickname String deriving (Show, Eq, Ord)

instance NFData PersonNickname where
  rnf (PersonNickname s) = rnf s

instance Pretty PersonNickname where
  pretty (PersonNickname s) = [s]

--------------------------------------------------------------------------------

newtype PersonBiography =
  PersonBiography String deriving (Show, Eq, Ord)

instance NFData PersonBiography where
  rnf (PersonBiography s) = rnf s

instance Pretty PersonBiography where
  pretty (PersonBiography s) = [s]

--------------------------------------------------------------------------------

newtype PersonRole =
  PersonRole String deriving (Show, Eq, Ord)

instance NFData PersonRole where
  rnf (PersonRole s) = rnf s

instance Pretty PersonRole where
  pretty (PersonRole s) = [s]

roleHeader :: [String]
roleHeader = ["Role"]
