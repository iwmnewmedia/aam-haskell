module AAM.Entities.Airforce where

import AAM.Pretty
import Data.Char
import Control.DeepSeq

--------------------------------------------------------------------------------

isAbbrChar :: Char -> Bool
isAbbrChar n = isUpper n || isDigit n

--------------------------------------------------------------------------------

data Airforce = Airforce String
  deriving (Show, Eq, Ord)

instance NFData Airforce where
  rnf (Airforce n) = rnf n

instance Pretty Airforce where
  pretty (Airforce n) = [n]

airforceKey :: Airforce -> String
airforceKey (Airforce n) = filter isAbbrChar n
