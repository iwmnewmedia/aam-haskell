{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module AAM.Util where

import AAM.Entities
import AAM.Data.Util
import Data.Bifunctor
import Data.Maybe (mapMaybe)
import Data.Tuple (swap)
import Data.Function (on)
import Data.List (sortBy, groupBy)
import Control.Applicative
import Control.Arrow ((&&&))
import Control.Monad.RWS
import Control.Monad.Writer

import qualified Data.Map as M
import qualified Data.Set as S

groupSortedBy :: Ord b => (a -> b) -> [a] -> [[a]]
groupSortedBy f = groupBy ((==) `on` f) . sortBy (compare `on` f)

-- Entity relationship utils ---------------------------------------------------

extractLinks :: (Ord v) => M.Map v (ID a) -> (v -> S.Set (R b)) -> [(R a, R b)]
extractLinks items f = go `concatMap` M.toList items
  where
  go (v, k) = (,) (Right k) `map` S.toList (f v)

removeReciprocals :: (Eq a) => [(R a, R a)] -> [(R a, R a)]
removeReciprocals = filter $ uncurry (/=)

-- Data field extract utils ----------------------------------------------------

extract :: (a -> R b) -> [ParseItem a] -> [ParseItem b]
extract f = mapMaybe go
  where
  go (ParseItem r x) = case f x of
    Left x' -> Just $ ParseItem r x'
    _ -> Nothing

extractMaybe :: (a -> Maybe (R b)) -> [ParseItem a] -> [ParseItem b]
extractMaybe f = mapMaybe go
  where
  go (ParseItem r x) = case f x of
    Just (Left x') -> Just $ ParseItem r x'
    _ -> Nothing

extractMulti :: (a -> S.Set (R b)) -> [ParseItem a] -> [ParseItem b]
extractMulti f = concatMap go
  where
  go (ParseItem r x) = go' r `mapMaybe` S.toList (f x)
  go' r (Left x) = Just $ ParseItem r x
  go' _ _ = Nothing

-- Duplicate removal -----------------------------------------------------------

sortedNub :: (Ord k) => [k] -> [k]
sortedNub = S.toList . S.fromList

sortedNubBy :: (Ord k) => (v -> k) -> [v] -> [v]
sortedNubBy f vs = M.elems $ M.fromList $ map (f &&& id) vs

-- Equivalent merging ----------------------------------------------------------

-- |
-- | Takes a list of items, a function that will attempt to merge items, and a
-- | function that extracts a key from each item.
-- |
mergeByKey :: (Show a, Eq a, Ord b)
           => (a -> a -> Either String a)
           -> (a -> b)
           -> [ParseItem a]
           -> ([String], [a])
mergeByKey merge f = mergeSomeByKey merge (Just . f)

mergeSomeByKey :: (Show a, Eq a, Ord b)
               => (a -> a -> Either String a)
               -> (a -> Maybe b)
               -> [ParseItem a]
               -> ([String], [a])
mergeSomeByKey merge f xs = second combineResult . swap . runWriter $ foldM go ([], M.empty) xs
  where
  combineResult (remain, result) = M.elems result ++ remain
  go (remain, result) (ParseItem pos record) =
    case f record of
      Nothing -> return (record : remain, result)
      Just k ->
        case k `M.lookup` result of
          Just x | record == x -> return (remain, result)
          Just x ->
            case merge x record of
              Left err -> tell [ "Merging record at " ++ show pos ++ " with existing record failed:\n\t" ++ err
                              ++ "\n\t" ++ show x
                              ++ "\n\t" ++ show record
                               ] >> return (record : remain, result)
              Right x' -> return (remain, M.insert k x' result)
          Nothing -> return (remain, M.insert k record result)


-- |
-- | A merge that always fails if called.
-- |
noMerge :: (Show a) => a -> a -> Either String a
noMerge x y = Left $ "No merge defined, but tried to merge:"
                  ++ "\n\t" ++ show x
                  ++ "\n\t" ++ show y

-- |
-- | A merge that always chooses the item on the left
-- |
leftMerge :: a -> a -> Either String a
leftMerge = const . Right

-- ID generation and substitution ----------------------------------------------

newtype Gen a = Gen { unGen :: RWS () [String] [Int] a }
  deriving (Functor, Applicative, Monad, MonadState [Int], MonadWriter [String])

runGen :: Gen a -> ([String], a)
runGen g = swap $ evalRWS (unGen g) () [1..]

runGen' :: Gen a -> a
runGen' g = fst $ evalRWS (unGen g) () [1..]

fresh :: Gen Int
fresh = do
  (s:ss) <- get
  put ss
  return s

genIDs :: (Ord a) => [a] -> M.Map a (ID a)
genIDs = M.fromList . runGen' . mapM go
  where
  go x = (,) x . ID <$> fresh

makeLookup :: (Ord b) => (a -> b) -> M.Map a (ID a) -> M.Map b (ID a)
makeLookup = M.mapKeys

substID' :: (Ord b, Show a, Show b) => (a -> b) -> M.Map b (ID a) -> R a -> Either String (R a)
substID' f ids (Left record) = maybe (Left $ "Lookup failed for " ++ show (f record)) (Right . Right) $ M.lookup (f record) ids
substID' _ _ i = Right i

substID :: (Ord b, Show a, Show b) => (a -> b) -> M.Map b (ID a) -> R a -> WS (Maybe (R a))
substID f ids = liftWS (substID' f ids)
