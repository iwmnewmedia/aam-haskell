{-# LANGUAGE FlexibleInstances #-}
module AAM.Pretty where

import Text.CSV (Record)

class Pretty a where
  pretty :: a -> Record

instance Pretty String where
  pretty = return

instance (Pretty a, Pretty b) => Pretty (a, b) where
  pretty (a, b) = pretty a ++ pretty b

instance (Show a, Pretty b) => Pretty (Either a b) where
  pretty (Left a) = error $ "Cannot pretty print Left value (" ++ show a ++ ")"
  pretty (Right b) = pretty b
