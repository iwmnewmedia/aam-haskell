module AAM.Parser where

import AAM.Entities
import AAM.Context
import Prelude hiding ((.), id)
import Control.Arrow
import Control.Applicative
import Control.Category
import Control.Monad (ap, mzero)
import Control.Parallel.Strategies
import Data.Char (toLower, toUpper)
import Data.List
import Data.Maybe (catMaybes)
import Text.CSV (CSV, Record, Field)
import qualified Data.Bifunctor as BF
import qualified Data.Text as T
import qualified Data.Map as M
import qualified Text.Parsec as P
import qualified Text.Parsec.Token as PT
import Text.Parsec.Language (emptyDef)

--------------------------------------------------------------------------------

type Header = Field
type Headers = [Header]
type ParseError = String

--------------------------------------------------------------------------------

newtype Parser a b = Parser { runParser :: (a, Context) -> ([ParseError], Maybe b) }

instance Monad (Parser a) where
  return x = Parser $ const ([], return x)
  f >>= g = Parser $ \(x, ctx) -> case runParser f (x, ctx) of
    (w1, Nothing) -> (w1, mzero)
    (w1, Just x') ->
      let (w2, result) = runParser (g x') (x, ctx)
      in (w1 ++ w2, result)

instance Applicative (Parser a) where
  pure = return
  (<*>) = ap

instance Functor (Parser a) where
  fmap f a = pure f <*> a

instance Category Parser where
  id = Parser $ \(x, _) -> ([], return x)
  f . g = Parser $ \(x, ctx) ->
    case runParser g (x, ctx) of
      (w1, Nothing) -> (w1, mzero)
      (w1, Just x') ->
        let (w2, result) = runParser f (x', ctx)
        in (w1 ++ w2, result)

instance Arrow Parser where
  first f = Parser $ \((b, d), ctx) -> case runParser f (b, ctx) of
    (w, Just c) -> (w, return (c, d))
    (w, Nothing) -> (w, mzero)
  arr f = Parser $ \(x, _) -> ([], return $ f x)

instance Alternative (Parser a) where
  empty = Parser $ const (["No alternative"], mzero)
  p <|> q = Parser $ \x ->
    case runParser p x of
      (_, Nothing) -> runParser q x
      (w, result) -> (w, result)

get :: Parser a a
get = Parser $ \(x, _) -> ([], return x)

ask :: Parser a Context
ask = Parser $ \(_, ctx) -> ([], return ctx)

tell :: String -> Parser a ()
tell msg = Parser $ const ([msg], return ())

tellFail :: String -> Parser a b
tellFail msg = Parser $ const ([msg], mzero)

------------------------------------------------------------------------------

rethrow :: String -> Parser a b -> Parser a b
rethrow msg p = Parser $ \x -> case runParser p x of
  (w, result) -> (((msg ++ ":\n") ++) `map` w, result)

infixl 5 <??>

(<??>) :: Parser a b -> String -> Parser a b
p <??> doc = rethrow ("Error parsing " ++ doc) p

(!?) :: [a] -> Int -> Either ParseError a
xs !? i | (i >= 0) && (i < length xs) = return $ xs !! i
_  !? i = Left $ "Record has no column " ++ show i

col :: Header -> Parser Record String
col name = Parser $ \(record, ctx) ->
  case name `elemIndex` headers ctx of
    Just i -> case record !? i of
      Left err -> ([err], mzero)
      Right x -> ([], return x)
    Nothing -> (["Unknown column " ++ show name], mzero)

cols :: [Header] -> Parser Record [String]
cols = mapM col

trim :: String -> String
trim = T.unpack . T.strip . T.pack

nonEmpty :: Parser [a] [a]
nonEmpty = get >>= \xs ->
  if null xs
    then tellFail "Value is empty"
    else return xs

option :: Parser a b -> Parser a (Maybe b)
option p = Parser $ \x -> BF.second Just (runParser p x)

tryAll :: [Parser a b] -> Parser a [b]
tryAll ps = Parser $ \x ->
  let (w, xs) = foldr (go x) ([], []) ps
  in (w, Just xs)
  where
  go x p (w1, result) = case runParser p x of
    (_, Nothing) -> (w1, result)
    (w2, Just x') -> (w1 ++ w2, x' : result)

-- |
-- | Absorb any errors that occur in parser `p`
-- |
try :: Parser a b -> Parser a b
try p = Parser $ \x -> case runParser p x of
  (_, Nothing) -> ([], Nothing)
  (_, x')      -> ([], x')

asInt :: Parser String Int
asInt = return . read =<< get

asDouble :: Parser String Double
asDouble = return . read =<< get

------------------------------------------------------------------------------

getRanks :: Parser a [String]
getRanks = do
  ctx <- ask
  let mkList f = map fst . M.toList . f $ ctx
  return $ mkList ranks

rankLookup :: String -> Parser a (R Rank)
rankLookup r = do
  ctx <- ask
  case r `M.lookup` ranks ctx of
    Nothing -> tellFail $ "Unknown rank '" ++ r ++ "'"
    Just x -> return $ Right x

------------------------------------------------------------------------------

lexer :: PT.TokenParser ()
lexer = PT.makeTokenParser emptyDef

signed :: (Num n) => PS n -> PS n
signed p = P.try (P.char '-' >> p >>= return . negate) <|> p

float :: PS Double
float = signed $ PT.float lexer

int :: PS Int
int = fromInteger <$> signed (PT.natural lexer)

lexeme :: PS a -> PS a
lexeme = PT.lexeme lexer

parens :: PS a -> PS a
parens = PT.parens lexer

comma :: PS String
comma = PT.comma lexer

type PS = P.Parsec String ()

properWord :: PS String
properWord = P.try properCase <|> P.try allUpper <|> P.try allLower <|> camelCase
  where
  properCase = do
    x <- P.upper
    ys <- P.many1 P.lower
    return (x : ys)
  allUpper = do
    x <- P.upper
    ys <- P.many1 P.upper
    return (x : toLower `map` ys)
  allLower = do
    x <- P.lower
    ys <- P.many1 P.lower
    return (toUpper x : ys)
  camelCase :: PS String
  camelCase = (:) <$> P.letter <*> P.many1 P.letter

properCamelCase :: PS String
properCamelCase = (:) <$> P.upper <*> P.many1 P.letter

------------------------------------------------------------------------------

parsec :: PS a -> Parser String a
parsec p = get >>= \x -> case P.parse p "" x of
  Left err -> tellFail $ "Parsing value " ++ show x ++ " failed: " ++ show err
  Right x' -> return x'

------------------------------------------------------------------------------

parseCSV :: (NFData a) => Context -> Parser Record a -> String -> CSV -> ([ParseError], [ParseItem a])
parseCSV _ _ _ [] = ([], [])
parseCSV ctx p filename (x:xs) = (concat errs, catMaybes results)
  where
  (errs, results) = unzip $ parMap rdeepseq go (zip xs [2..])
  go ([""], _) = ([], Nothing)
  go (record, i) =
      let (errs', result) = runParser (rethrow ("Error parsing " ++ filename ++ " at row " ++ show (i :: Int)) p) (record, ctx { headers = x })
      in (errs', ParseItem (Pos filename i) <$> result)
