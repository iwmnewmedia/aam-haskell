module AAM.Sources.PaulAndrews (parse) where

import Prelude hiding ((.), id)
import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Arrow (arr)
import Control.Applicative
import Control.Category
import Data.Bifunctor
import Data.Either (lefts)
import Data.Function (on)
import Data.List (unzip5, sortBy, isPrefixOf)
import Data.Maybe (catMaybes, fromMaybe)
import Data.Time (Day, parseTime)
import Text.CSV (CSV, Record)
import System.Locale (defaultTimeLocale)
import qualified Data.Set as S
import qualified Text.Parsec as P

--------------------------------------------------------------------------------

parseName :: [String] -> PS (Maybe String, PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix)
parseName rankList = do
  let rankList' = sortBy (flip compare `on` length) $ filter (not . null) rankList
  rank      <- P.optionMaybe (lexeme (P.choice $ (P.try . P.string) `map` rankList')) P.<?> "rank"
  firstName <- PersonFirstName <$> lexeme firstNamePart P.<?> "first name"
  initial   <- P.optionMaybe (PersonInitial . unwords <$> (P.many1 . P.try $ lexeme initialPart)) P.<?> "initial"
  lastName  <- PersonLastName <$> lexeme lastNamePart P.<?> "last name"
  suffix    <- P.optionMaybe (P.try $ lexeme suffixPart) P.<?> "suffix"
  _ <- P.spaces
  _ <- P.eof
  return (rank, firstName, initial, lastName, PersonNameSuffix <$> suffix)
  where

  firstNamePart :: PS String
  firstNamePart = P.try (P.string "La Mar")
              <|> P.try (return <$> P.upper <* (P.char '.' <|> P.space))
              <|> P.try (P.many1 (P.oneOf ".?") *> return "")
              <|> properCamelCase

  initialPart :: PS String
  initialPart = P.try (return <$> P.upper <* (P.char '.' <|> P.space))
            <|> (lexeme properWord <* P.lookAhead (P.try lastNamePart))

  lastNamePart :: PS String
  lastNamePart = P.try ((++) <$> P.string "Van " <*> properWord)
             <|> P.try ((++) <$> P.string "Vander " <*> properWord)
             <|> P.try ((++) <$> P.string "Von " <*> properWord)
             <|> P.try ((++) <$> P.string "Del " <*> properWord)
             <|> P.try ((++) <$> P.string "Des " <*> properWord)
             <|> P.try ((++) <$> P.string "De " <*> properWord)
             <|> P.try ((++) <$> P.string "De'" <*> properWord)
             <|> P.try ((++) <$> P.string "Du " <*> properWord)
             <|> P.try ((++) <$> P.string "D'" <*> properWord)
             <|> P.try ((++) <$> P.string "E'" <*> properWord)
             <|> P.try ((++) <$> P.string "O'" <*> properWord)
             <|> P.try ((++) <$> P.string "L'" <*> properWord)
             <|> P.try ((++) <$> P.string "Le " <*> properWord)
             <|> P.try ((++) <$> P.string "St." <* P.optional P.space *> properWord)
             <|> P.try ((++) <$> P.string "St " <*> properWord)
             <|> P.try ((++) <$> P.string "Ten " <*> properWord)
             <|> P.try ((++) <$> P.string "San " <*> properWord)
             <|> P.try ((++) <$> P.string "Sam " <*> properWord)
             <|> P.try ((++) <$> properWord <*> ((++) <$> (return <$> P.char '-') <*> properWord))
             <|> properCamelCase

  suffixPart :: PS String
  suffixPart = P.string "Jr" <|> P.string "Sr" <|> P.string "III" <|> P.string "II"

--------------------------------------------------------------------------------

parseDate :: Parser String (Maybe Day)
parseDate = arr $ parseTime defaultTimeLocale "%e-%b-%Y"

--------------------------------------------------------------------------------

parseAircraftEvent :: Parser Record (R Aircraft -> Event Aircraft)
parseAircraftEvent = Event
  <$> pure False
  <*> nonEmpty . arr trim . col "40 - Event"
  <*> pure Nothing
  <*> parseDate . col "34 - Date of Event"
  <*> option (locationName <$> nonEmpty . arr trim . col "53 - Geographic area")

--------------------------------------------------------------------------------

parsePersonEvent :: Parser Record (R Person -> Event Person)
parsePersonEvent = Event
  <$> pure False
  <*> nonEmpty . arr trim . col "56 - Fate" <??> "fate"
  <*> pure Nothing
  <*> parseDate . col "34 - Date of Event" <??> "date"
  <*> option (locationName <$> nonEmpty . arr trim . col "53 - Geographic area" <??> "place")

--------------------------------------------------------------------------------

parsePersonHometown :: Parser Record (R Person -> Event Person)
parsePersonHometown = Event
  <$> pure True
  <*> pure "Born"
  <*> pure Nothing
  <*> pure Nothing
  <*> option (locationName <$> nonEmpty . arr trim . col "57 - Hometown" <??> "hometown")

--------------------------------------------------------------------------------

parseAircraft :: S.Set SourceMsg
              -> S.Set (R Airforce)
              -> S.Set (R Unit)
              -> Parser Record Aircraft

parseAircraft src af unit =
  Aircraft <$> (AircraftSerial <$> nonEmpty . arr trim . col "04 - Serial" <??> "serial")
           <*> (Just . Left . AircraftModel <$> nonEmpty . arr trim . col "02-ACType" <??> "model")
           <*> option (AircraftBlockNum <$> nonEmpty . arr trim . col "03B-Manufacture Block Number" <??> "block number")
           <*> option (Left . AircraftManufacturer <$> nonEmpty . arr trim . col "03A-Manufacturer " <??> "manufacturer")
           <*> pure Nothing
           <*> pure af
           <*> pure unit
           <*> pure src

findAirforce :: Parser Record (S.Set (R Airforce))
findAirforce = S.fromList <$> tryAll [ Left . Airforce <$> nonEmpty . arr trim . col "12 - Air Force" <??> "air force" ]

findUnits :: S.Set SourceMsg -> S.Set (R Airforce) -> Parser Record [R Unit]
findUnits src afs = do
  units <- tryAll
    [ mkSquadron <$> nonEmpty . arr trim . col "17 - Squadron" <??> "squadron"
    , mkUnit (Just Group)    <$> nonEmpty . arr trim . col "16 - Group" <??> "group"
    , mkUnit (Just Wing)     <$> nonEmpty . arr trim . col "15 - Wing" <??> "wing"
    , mkUnit (Just Division) <$> nonEmpty . arr trim . col "14 - Division" <??> "division"
    , mkUnit (Just Command)  <$> nonEmpty . arr trim . col "13 - Command" <??> "command" ]
  return $ map (first $ \u -> u { uAirforce = afs, uSource = src }) units
  where
  mkSquadron name
    | "Headquarters (" `isPrefixOf` name = mkUnit (Just Headquarters) name
    | otherwise = mkUnit (Just Squadron) name

parseSources :: Parser Record (S.Set SourceMsg)
parseSources = S.fromList . map SourceRef <$> tryAll
           [ ("AAIR " ++) <$> nonEmpty . arr trim . col "69 - Aircraft Accident and Incident Report Number"
           , ("E&E "  ++) <$> nonEmpty . arr trim . col "70 - Escape &Evasion Report Number"
           , ("MACR " ++) <$> nonEmpty . arr trim . col "72 - Missing Air Crew Report Number"
           ]

parsePerson :: S.Set SourceMsg
            -> S.Set (R Airforce)
            -> S.Set (R Unit)
            -> S.Set (R Aircraft)
            -> Parser Record Person

parsePerson src af unit ac = do
  rankList <- getRanks
  (rank, firstName, initial, lastName, suffix) <- (parsec (parseName rankList) . try nonEmpty . arr trim . col "55 Rank and Name") <??> "rank and name"
  rankId <- maybe (return Nothing) (option . rankLookup) rank
  role <- S.fromList <$> tryAll [ PersonRole <$> nonEmpty . arr trim . col "54 - Position" ] <??> "position"
  return $ Person (Nothing, Nothing) rankId firstName initial lastName suffix Nothing role unit af ac Nothing S.empty src

parseRecord :: Parser Record (Maybe Aircraft,
                              Maybe (Event Aircraft),
                              Maybe Person,
                              [Event Person],
                              [Unit])
parseRecord = do

  src <- S.insert (SourceText "Paul Andrews, Project Bits and Pieces, 8th Air Force Roll of Honor database") <$> parseSources <??> "sources"
  af <- findAirforce <??> "airforce"
  units <- linkUnits . lefts <$> findUnits src af <??> "unit"
  let unit = S.fromList $ Left `map` ((\u -> fromMaybe (error "found Nothing unit type") (uType u) >= Group) `filter` units)
  aircraft      <- option . try $ parseAircraft src af unit <??> "aircraft"
  aircraftEvent <- option . try $ parseAircraftEvent <??> "aircraft event"
  person        <- option $ parsePerson src af unit (maybe S.empty (S.singleton . Left) aircraft) <??> "person"
  personEvent   <- option . try $ parsePersonEvent <??> "person event"
  personHometown <- option . try $ parsePersonHometown <??> "person event"

  let aircraftEvent' = maybe Nothing (\e -> ($ Left e) <$> aircraftEvent) aircraft
  let personEvent'   = maybe Nothing (\e -> ($ Left e) <$> personEvent) person
  let personHometown' = maybe Nothing (\e -> ($ Left e) <$> personHometown) person
  return (aircraft, aircraftEvent', person, catMaybes [personEvent', personHometown'], units)

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], ([ParseItem Aircraft],
                                      [ParseItem (Event Aircraft)],
                                      [ParseItem Person],
                                      [ParseItem (Event Person)],
                                      [ParseItem Unit]))
parse ctx filename csv = (errors, (catMaybes acs, catMaybes aces, catMaybes ps, concat pes, concat us))
  where
  (errors, rows) = parseCSV ctx parseRecord filename csv
  (acs, aces, ps, pes, us) = unzip5 (rewrap `map` rows)
  rewrap (ParseItem r (ac, ace, p, pe, u)) =
    (ParseItem r <$> ac,
     ParseItem r <$> ace,
     ParseItem r <$> p,
     ParseItem r <$> pe,
     ParseItem r <$> u)
