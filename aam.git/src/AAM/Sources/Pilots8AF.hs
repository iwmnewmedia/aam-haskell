module AAM.Sources.Pilots8AF (parse) where

import Prelude hiding ((.), id)
import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Arrow (arr)
import Control.Applicative
import Control.Category
import Data.List (isPrefixOf, isSuffixOf)
import Data.Maybe (catMaybes)
import Data.Time (Day, parseTime)
import Text.CSV (CSV, Record)
import System.Locale (defaultTimeLocale)
import qualified Data.Set as S
import qualified Text.Parsec as P

--------------------------------------------------------------------------------

parseName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix)
parseName = P.try lastNameOnly <|> do
  lastName <- lastNameValue P.<?> "last name"
  P.char ','
  P.spaces
  firstName <- PersonFirstName <$> lexeme firstNamePart P.<?> "first name"
  initial <- initialValue P.<?> "initial"
  suffix <- P.optionMaybe (PersonNameSuffix <$> lexeme suffixPart) P.<?> "suffix"
  optional $ lexeme $ P.char '*'
  P.eof
  return (firstName, initial, lastName, suffix)

  where
  lastNameOnly = do
    lastName <- lastNameValue
    optional $ P.char ','
    P.eof
    return (PersonFirstName "", Nothing, lastName, Nothing)
  lastNameValue = PersonLastName . unwords <$> P.many1 (lexeme lastNamePart) P.<?> "last name"
  lastNamePart = P.try ((++) <$> P.string "O'" <*> properWord)
             <|> P.try ((++) <$> P.string "De " <*> properWord)
             <|> P.try ((++) <$> P.string "L'" <*> properWord)
             <|> P.try ((++) <$> properWord <*> ((++) <$> (return <$> P.char '-') <*> properWord))
             <|> properWord
  firstNamePart = P.try ((++) <$> P.string "Le " <*> properWord)
              <|> P.try ((++) <$> P.string "Le" <*> properWord)
              <|> P.try ((++) <$> P.string "De " <*> properWord)
              <|> P.try ((++) <$> P.string "De" <*> properWord)
              <|> P.try ((++) <$> P.string "Mc" <*> properWord)
              <|> P.try (P.string "Billy Joe")
              <|> P.try properWord
              <|> return <$> P.upper
  initialValue = P.try (lexeme (P.string "(NMI)") *> return Nothing)
             <|> P.optionMaybe (PersonInitial <$> P.many1 (P.try $ lexeme initialPart))
  suffixPart = P.try (P.string "5th")
           <|> P.try (P.string "Sr")
           <|> P.try (P.string "Jr")
           <|> P.try (P.string "IV")
           <|> P.try (P.string "III")
           <|> P.string "II"

  bit :: PS String
  bit = do
    x <- P.upper
    ys <- P.many P.letter
    return (x : ys)

  initialPart :: PS Char
  initialPart = do
    (i : xs) <- bit
    P.optional $ P.char '.'
    if not (null xs)
      then P.unexpected $ "Non-initial string '" ++ (i : xs) ++ "'"
      else return i


------------------------------------------------------------------------------

parseDate :: Parser String (Maybe Day)
parseDate = arr $ parseTime defaultTimeLocale "%e-%b-%Y"

------------------------------------------------------------------------------

parsePersonEvent :: Parser Record (R Person -> Event Person)
parsePersonEvent = Event
  <$> pure False
  <*> try (nonEmpty . arr trim . col "Event")
  <*> parseDate . col "Assigned"
  <*> parseDate . col "End date"
  <*> pure Nothing

------------------------------------------------------------------------------

parsePerson :: Parser Record Person
parsePerson = do
  (firstName, initial, lastName, suffix) <- parsec parseName . nonEmpty . arr trim . col "Last Name, First Name " <??> "name"
  serviceNum <- try $ mkServiceNumber <$> option (nonEmpty . arr trim . col "Army Air Force serial number") <??> "Service number"
  rank <- parseRank
  unit <- S.fromList <$> tryAll [ mkGroup <$> nonEmpty . arr trim . col "Group"
                                , mkSquadron <$> nonEmpty . arr trim . col "Squadron" ]
  return $ Person serviceNum rank firstName initial lastName suffix Nothing S.empty unit airforce S.empty Nothing S.empty src
  where
  airforce = S.singleton $ Left $ Airforce "8th Air Force"
  src = S.singleton $ SourceText "Ted Damick, VIII Fighter Command pilots list"
  mkGroup name =
    if "Command" `isSuffixOf` name
    then mkUnit (Just Command) name
    else
      if "Wing" `isSuffixOf` name
      then mkUnit (Just Wing) name
      else mkUnit (Just Group) name
  mkSquadron name =
    if "Headquarters (" `isPrefixOf` name
    then mkUnit (Just Headquarters) name
    else mkUnit (Just Squadron) name

parseRank :: Parser Record (Maybe (R Rank))
parseRank = do
  rank <- try . option $ nonEmpty . arr trim . col "Rank" <??> "rank"
  maybe (return Nothing) (\r -> Just <$> rankLookup r) rank

parseRecord :: Parser Record (Person, Maybe (Event Person))
parseRecord = do
  person <- parsePerson <??> "person"
  personEvent <- option $ parsePersonEvent <??> "person event"
  let personEvent' = ($ Left person) <$> personEvent
  return (person, personEvent')

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], ([ParseItem Person],
                                      [ParseItem (Event Person)]))
parse ctx filename csv = (errors, (ps, catMaybes pes))
  where
  (errors, rows) = parseCSV ctx parseRecord filename csv
  (ps, pes) = unzip (rewrap `map` rows)
  rewrap (ParseItem r (p, pe)) = (ParseItem r p, ParseItem r <$> pe)
