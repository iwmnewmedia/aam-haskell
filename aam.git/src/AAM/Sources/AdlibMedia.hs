module AAM.Sources.AdlibMedia (parse, extractPeople) where

import Prelude hiding ((.))

import AAM.Context
import AAM.Entities
import AAM.Parser
import AAM.Util (sortedNub)
import Control.Arrow (arr)
import Control.Applicative
import Control.Category
import Control.Parallel.Strategies
import Data.Function (on)
import Data.List (find, isPrefixOf, sortBy, intercalate)
import Text.CSV (CSV, Record)
import qualified Data.Text as T
import qualified Text.Parsec as P

--------------------------------------------------------------------------------

parseRecord :: Parser Record (AdlibMedia, ID AdlibMedia)
parseRecord = do
  mediaId <- ID <$> asInt . nonEmpty . arr trim . col "id" <??> "ID"
  caption <- Caption . T.pack <$> nonEmpty . arr trim . col "caption" <??> "caption"
  place <- option $ try (PlaceName <$> nonEmpty . arr trim . col "place" <??> "place")
  accession <- AccessionNumber <$> nonEmpty . arr trim . col "accession_number" <??> "accession number"
  return (AdlibMedia caption place accession, mediaId)

--------------------------------------------------------------------------------

extractPeople :: [(String, ID Rank)] -> [(AdlibMedia, ID AdlibMedia)] -> [((ID Rank, String), ID AdlibMedia)]
extractPeople rankList = sortedNub . concat . parMap rdeepseq go
  where
  go :: (AdlibMedia, ID AdlibMedia) -> [((ID Rank, String), ID AdlibMedia)]
  go (media, i) = (\x -> (x, i)) <$> findPeople rankList' (amCaption media)

  rankList' :: [(String, ID Rank)]
  rankList' = sortBy (flip compare `on` (length . fst))
            . concatMap addDot
            . filter (not . null . fst)
            $ rankList

  addDot :: (String, ID Rank) -> [(String, ID Rank)]
  addDot (r, i) = [(r, i), (intercalate ". " (words r) ++ ".", i)]

findPeople :: [(String, ID Rank)] -> Caption -> [(ID Rank, String)]
findPeople rankList (Caption text) = scanCaption $ T.unpack text
  where

  scanCaption :: String -> [(ID Rank, String)]
  scanCaption = flip go []
    where
    go :: String -> [(ID Rank, String)] -> [(ID Rank, String)]
    go [] result = result
    go s result = case find ((`isPrefixOf` s) . fst) rankList of
      Nothing -> go (drop 1 s) result
      Just (x, i) ->
        let s' = drop (length x) s
        in case P.runParser parseName () "" s' of
          Left _ -> go s' result
          Right name -> go s' $ (i, name) : result

    parseName :: PS String
    parseName = unwords <$> (P.spaces *> P.many1 (lexeme namePart))

    namePart :: PS String
    namePart = P.try properCase <|> (return <$> P.upper <* optional (P.oneOf ".") <* P.space)

    properCase :: PS String
    properCase = do
      x <- P.upper
      ys <- P.many1 P.lower
      return (x : ys)

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], [ParseItem (AdlibMedia, ID AdlibMedia)])
parse = flip parseCSV parseRecord
