module AAM.Sources.UnitsDatabase (parse) where

import Prelude hiding (id, (.))

import AAM.Context
import AAM.Entities
import AAM.Parser
import Data.Bifunctor
import Data.Either (lefts)
import Data.Maybe (fromMaybe)
import Control.Applicative
import Control.Arrow (arr)
import Control.Category
import Text.CSV (CSV, Record)

import qualified Data.Set as S

--------------------------------------------------------------------------------

findUnits :: S.Set SourceMsg -> S.Set (R Airforce) -> Parser Record [R Unit]
findUnits src afs = do
  units <- tryAll
    [ parseUnit Command      "Command"
    , parseUnit Division     "Division"
    , parseUnit Wing         "Wing"
    , parseUnit Brigade      "Brigade"
    , parseUnit Regiment     "Regiment"
    , parseUnit Group        "Group"
    , parseUnit Squadron     "Squadron"
    , parseUnit Headquarters "Headquarters"
    , parseUnit Battalion    "Battalion"
    , parseUnit Company      "Company"
    , parseUnit Platoon      "Platoon"
    , parseUnit Section      "Section"
    , parseUnit Misc         "Misc"
    , parseUnit UnitUnit     "Unit"
    , parseUnit MedicalUnit  "Medical Unit"
    , parseUnit Detachment   "Detachment"
    ]
  return $ map (first $ \u -> u { uAirforce = afs, uSource = src }) units
  where
  parseUnit ty name = mkUnit (Just ty) <$> nonEmpty . arr trim . col name

--------------------------------------------------------------------------------

parseRecord :: Parser Record [Unit]
parseRecord = do
  src <- S.fromList <$> tryAll [ SourceText <$> nonEmpty . arr trim . col "Source" ]
  airforce <- S.fromList <$> tryAll [ Left . Airforce <$> nonEmpty . arr trim . col "Air Force" ]
  place <- S.fromList <$> tryAll [ Left . mkPlace <$> nonEmpty . arr trim . col "Location" ]
  result <- linkUnits . lefts <$> findUnits src airforce
  return $ linkPlace place `map` result
  where
    linkPlace place u =
      let ut = fromMaybe (error "Nothing in unit type") (uType u)
      in if ut >= Group then u { uPlace = place } else u

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], [ParseItem Unit])
parse ctx filename csv = (errs, concatMap go units)
  where
  (errs, units) = parseCSV ctx parseRecord filename csv
  go (ParseItem src us) = ParseItem src <$> us
