module AAM.Sources.Missions (parse) where

import Prelude hiding ((.))

import AAM.Context
import AAM.Entities
import AAM.Parser as P
import Control.Applicative
import Control.Arrow (arr)
import Control.Category
import Data.List (isInfixOf)
import Data.Text (pack, unpack, split)
import Data.Time (Day, parseTime, fromGregorian, toGregorian)
import System.Locale (defaultTimeLocale)
import Text.CSV (CSV, Record)
import qualified Data.Set as S

dataSource :: SourceMsg
dataSource = SourceText "Lee Cunningham, 8th Air Force missions research database"

--------------------------------------------------------------------------------

parseRecord :: Parser Record (Mission, MissionEntry)
parseRecord = do
  src <- S.insert dataSource <$> (S.fromList . map SourceText <$> tryAll [ nonEmpty . arr trim . col "References" ])
  m <- Mission . MissionName <$> nonEmpty . arr trim . col "Mission name/ number"
                             <*> parseDate . nonEmpty . arr trim . col "Mission date"
                             <*> pure src
  me <- MissionEntry <$> pure (Left m)
                     <*> parseTarget
                     <*> (fmap MissionDescription <$> arr notNYK . (option . try $ nonEmpty . arr trim . col "DESCRIPTION 1"))
                     <*> (option . try $ Left . AircraftModel <$> nonEmpty . arr trim . col "Aircraft type participating")
                     <*> parseUnits
                     <*> (option . try $ MissionEntryNotes <$> nonEmpty . arr trim . col "Notes")
                     <*> (arr notNYK . (option . try $ nonEmpty . arr trim . col "Tonnage dropped"))
                     <*> cols [ "Number of aircraft Sent"
                              , "Number of aircraft Effective"
                              , "Number of aircraft Missing In Action"
                              , "Number of aircraft Damaged Beyond Repair"
                              , "Number of aircraft Damaged"
                              , "Number of people Killed In Action"
                              , "Number of people Wounded in Action"
                              , "Number of people Missing In Action"
                              , "Number of people Evaded"
                              , "Number of people Prisoners of War"
                              , "Number of people Died in Captivity"
                              , "Number of people Interned"
                              , "Number of people Returned To Duty"
                              , "Enemy aircraft claimed as Destroyed by Bomber Command"
                              , "Enemy aircraft claimed as Probably Destroyed by Bomber Command"
                              , "Enemy aircraft claimed as Damaged by Bomber Command"
                              , "Enemy aircraft claimed as Destroyed by Fighter Command"
                              , "Enemy aircraft claimed as Probably Destroyed by Fighter Command"
                              , "Enemy aircraft claimed as Damaged by Fighter Command"
                              ]
  return (m, me)
  where
  notNYK :: Maybe String -> Maybe String
  notNYK (Just "Not yet known") = Nothing
  notNYK x = x

parseDate :: Parser String Day
parseDate = fixCentury <$> (try parseShortDate <|> parseFullDate)
  where fixCentury = (\(y, m, d) -> fromGregorian (y - 100) m d) . toGregorian

parseShortDate :: Parser String Day
parseShortDate = get >>= maybe err return . parseTime defaultTimeLocale "%y%m%d"
  where err = tellFail "Short date parse failed"

parseFullDate :: Parser String Day
parseFullDate = get >>= maybe err return . parseTime defaultTimeLocale "%e-%b-%y"
  where err = tellFail "Full date parse failed"

parseTarget :: Parser Record (S.Set (R MissionEntryTarget))
parseTarget = try parseTargetList <|> pure S.empty
  where
  parseTargetList = S.fromList <$> arr splitTargets . nonEmpty . arr trim . col "Associated location"
  splitTargets = map (Left . MissionEntryTarget) . filterNYK . map (trim . unpack) . split (== ',') . pack

filterNYK :: [String] -> [String]
filterNYK = filter (not . (== "Not yet known"))

parseUnits :: Parser Record (S.Set (R Unit))
parseUnits = S.fromList <$> tryAll [ mkUnit' . nonEmpty . arr trim . col ("Associated unit " ++ show (n :: Int)) | n <- [1..19] ]
  where
  mkUnit' :: Parser String (R Unit)
  mkUnit' = do
    name <- get
    case name of
      _ | "Division"    `isInfixOf` name -> return $ mkUnit (Just Division) name
      _ | "Wing"        `isInfixOf` name -> return $ mkUnit (Just Wing) name
      _ | "Group"       `isInfixOf` name -> return $ mkUnit (Just Group) name
      _ | "Squadron"    `isInfixOf` name -> return $ mkUnit (Just Squadron) name
      _ | "Combat Crew" `isInfixOf` name -> return $ mkUnit (Just Group) name
      "Not yet known" -> tellFail "Unknown unit value"
      "Unknown" -> tellFail "Unknown unit value"
      _ -> error $ "Unknown unit type: " ++ name

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], [(ParseItem Mission, ParseItem MissionEntry)])
parse ctx filename csv = (errs, rewrap `map` items)
  where
  (errs, items) = parseCSV ctx parseRecord filename csv
  rewrap (ParseItem r (x, y)) = (ParseItem r x, ParseItem r y)
