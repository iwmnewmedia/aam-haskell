module AAM.Sources.GroupDescription (parse) where

import Prelude hiding ((.))

import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Applicative
import Control.Arrow (arr)
import Control.Category
import Data.List (isInfixOf)
import Text.CSV (CSV, Record)
import qualified Data.Set as S
import qualified Text.Parsec as P

--------------------------------------------------------------------------------

parseRecord :: Parser Record Unit
parseRecord = do
  name <- parsec parseName . col "Name of Group"
  desc <- option $ try (UnitDescription <$> nonEmpty . arr trim . col "Description of Group")
  src <- S.fromList <$> tryAll [ SourceText <$> nonEmpty . arr trim . col "Sources" <??> "source" ]
  let utype = Just $ if "Squadron" `isInfixOf` name then Squadron else Group
  return $ Unit utype (UnitName name) S.empty S.empty S.empty desc S.empty src

--------------------------------------------------------------------------------

number :: PS Int
number = P.many1 P.digit >>= return . read

numberSuffix :: PS String
numberSuffix = foldr1 (<|>) (P.string `map` ["st", "nd", "rd", "th"])

nameWord :: PS String
nameWord = (:) <$> P.upper <*> P.many P.lower

nameWords :: PS String
nameWords = P.many1 (lexeme nameWord) >>= return . unwords

numberPart :: PS (Int, String)
numberPart = do
  num <- lexeme number
  suffix <- lexeme numberSuffix
  return (num, suffix)

parseName :: PS String
parseName = do
  np <- P.try (P.optionMaybe numberPart P.<?> "Number part")
  title <- nameWords P.<?> "Words"
  case np of
    Just (num, suffix) -> return $ show num ++ suffix ++ " " ++ title
    Nothing -> return title

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], [ParseItem Unit])
parse ctx = parseCSV ctx parseRecord
