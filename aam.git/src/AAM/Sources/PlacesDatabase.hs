module AAM.Sources.PlacesDatabase (parse) where

import Prelude hiding ((.))

import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Arrow (arr)
import Control.Applicative
import Control.Category
import Data.Text (pack, split, strip, unpack)
import Text.CSV (CSV, Record)

import qualified Data.Set as S

--------------------------------------------------------------------------------

parseLocation :: Parser String Location
parseLocation = parsec $
  Location <$> (Latitude <$> float)
           <*> (comma *> (Longitude <$> float))

--------------------------------------------------------------------------------

parseRecord :: Parser Record Place
parseRecord = Place
  <$> option (StationNumber <$> try nonEmpty . arr trim . col "Station number") <??> "Station number"
  <*> (PlaceName <$> nonEmpty . arr trim . col "Station name") <??> "Station name"
  <*> (S.fromList <$> (akaNames <|> pure [])) <??> "Alternative names"
  <*> (option . try $ parseLocation . nonEmpty . arr trim . col "Lat/Long") <??> "Location"
  <*> (Just . PlaceDescription <$> arr trim . col "Description") <??> "Description"
  <*> parseAirfieldFlag <??> "Airfield flag"
  <*> (S.fromList <$> tryAll [ af8, af9 ])
  <*> (S.fromList . map SourceText <$> tryAll [ nonEmpty . arr trim . col "References 1"
                                              , nonEmpty . arr trim . col "References 2" ]) <??> "References"

  where

  parseAirfieldFlag = do
    x <- arr trim . col "Airfield (Y/N)"
    return . Just $ if x == "Y" then PlaceAirfield else PlaceMilitary

  akaNames = do
    x <- nonEmpty . arr trim . col "Notes"
    let t = (unpack . strip) `map` split (== ';') (pack x)
    return $ PlaceName `map` t

  af8 = (nonEmpty . arr trim . col "8th AF") *> pure (Left $ Airforce "8th Air Force")
  af9 = (nonEmpty . arr trim . col "9th AF") *> pure (Left $ Airforce "9th Air Force")

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], [ParseItem Place])
parse ctx = parseCSV ctx parseRecord
