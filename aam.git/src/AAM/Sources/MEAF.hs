module AAM.Sources.MEAF where

import Prelude hiding (sequence, (.), mapM)

import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Applicative
import Control.Arrow (arr, (&&&))
import Control.Category
import Control.Monad (join)
import Data.Bifunctor (second)
import Data.Char
import Data.List (intercalate, isInfixOf, isPrefixOf)
import Data.Maybe (catMaybes, isNothing)
import Data.Traversable
import Data.Time (Day, parseTime)
import Text.CSV (CSV, Record)
import System.Locale (defaultTimeLocale)
import qualified Data.Set as S
import qualified Text.Parsec as P
import qualified Data.Text as T

--------------------------------------------------------------------------------

sep :: PS ()
sep = P.many (P.try (P.oneOf ".,") <|> P.space) *> return ()

decap :: String -> String
decap name | length name == 1 = name
           | all isUpper name = head name : (toLower `map` tail name)
           | otherwise = name

bit :: PS String
bit = do
  x <- P.upper
  ys <- P.many P.letter
  return (x : ys)

lastNamePart :: PS PersonLastName
lastNamePart = PersonLastName . decap <$> (P.try prefixed <|> namePart) P.<?> "last name"
  where
  prefixed = do
    x <- P.try (P.string "De La ")
     <|> P.try (P.string "De Los ")
     <|> P.try (P.string "De ")
     <|> P.try (P.string "Del ")
     <|> P.try (P.string "Des ")
     <|> P.try (P.string "Di ")
     <|> P.try (P.string "La ")
     <|> P.try (P.string "Le ")
     <|> P.try (P.string "L ")
     <|> P.try (P.string "St. ")
     <|> P.try (P.string "St ")
     <|> P.try (P.string "Santa ")
     <|> P.try (P.string "Mac ")
     <|> P.try (P.string "Von ")
     <|> P.string "Van De "
     <|> P.string "Van Der "
     <|> P.string "Van "
    ys <- properWord
    return (x ++ ys)

namePart :: PS String
namePart = do
  x <- P.letter
  ys <- P.many1 $ P.try P.letter <|> P.try (P.char '\'') <|> P.char '-'
  return (x : ys)

initialPart :: PS Char
initialPart = do
  (i : xs) <- bit
  P.optional $ P.char '.'
  if not (null xs)
    then P.unexpected $ "Non-initial string '" ++ (i : xs) ++ "'"
    else return i

initialsBlock :: PS (Maybe PersonInitial)
initialsBlock = (P.try (nmi *> return Nothing)
            <|> (Just . PersonInitial . decap <$> (P.many1 . P.try $ lexeme initialPart))) P.<?> "initial"

suffixPart :: PS (Maybe PersonNameSuffix)
suffixPart = P.optionMaybe (PersonNameSuffix <$> pattern) P.<?> "suffix"
  where
  pattern = lexeme $ P.try (P.string "Jr.")
                 <|> P.try (P.string "Jr")
                 <|> P.try (P.string "Sr.")
                 <|> P.try (P.string "Sr")
                 <|> P.try (P.string "Rev.")
                 <|> P.try (P.string "Rev")
                 <|> P.try (P.string "Dr.")
                 <|> P.try (P.string "Dr")
                 <|> P.try (P.string "IV.")
                 <|> P.try (P.string "IV")
                 <|> P.try (P.string "III.")
                 <|> P.try (P.string "III")
                 <|> P.try (P.string "II.")
                 <|> P.string "II"

nicknamePart :: PS PersonNickname
nicknamePart = PersonNickname . decap <$> lexeme (P.between quot' quot' (P.many $ P.noneOf "\"'"))
  where quot' = P.oneOf "\"'"

randomNumberPart :: PS ()
randomNumberPart = lexeme $ P.many P.digit *> return ()

nmi :: PS ()
nmi = (P.try (P.string "(NMI)")
   <|> P.try (P.string "NMI")
   <|> P.try (P.string "(nmi)")
   <|> P.string "nmi") *> return ()

basicName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
basicName = do
  lastName <- lexeme lastNamePart
  sep
  firstName <- PersonFirstName . decap <$> lexeme namePart P.<?> "first name"
  sep
  nickname1 <- P.optionMaybe nicknamePart
  P.optional $ P.try nmi
  sep
  nickname2 <- P.optionMaybe nicknamePart
  suffix <- suffixPart
  nickname3 <- P.optionMaybe nicknamePart
  randomNumberPart
  P.eof
  return (firstName, Nothing, lastName, suffix, nickname1 <|> nickname2 <|> nickname3)

lastNameOnly :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
lastNameOnly = do
  lastName <- lexeme lastNamePart
  sep
  P.eof
  return (PersonFirstName "", Nothing, lastName, Nothing, Nothing)

fullName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
fullName = do
  lastName <- lexeme lastNamePart
  sep
  firstName  <- PersonFirstName . decap <$> lexeme namePart P.<?> "first name"
  sep
  nickname1 <- P.optionMaybe nicknamePart
  initial <- PersonInitial . unwords <$> (P.many1 . P.try $ lexeme namePart) P.<?> "initial"
  sep
  nickname2 <- P.optionMaybe nicknamePart
  suffix <- suffixPart
  nickname3 <- P.optionMaybe nicknamePart
  randomNumberPart
  P.eof
  return (firstName, Just initial, lastName, suffix, nickname1 <|> nickname2 <|> nickname3)

initialedBasicName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
initialedBasicName = do
  lastName <- lexeme lastNamePart
  sep
  firstName  <- PersonFirstName . decap <$> lexeme namePart P.<?> "first name"
  sep
  nickname1 <- P.optionMaybe nicknamePart
  initial <- initialsBlock
  sep
  nickname2 <- P.optionMaybe nicknamePart
  suffix <- suffixPart
  nickname3 <- P.optionMaybe nicknamePart
  randomNumberPart
  P.eof
  return (firstName, initial, lastName, suffix, nickname1 <|> nickname2 <|> nickname3)

initialedBasicNameReverse :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
initialedBasicNameReverse = do
  lastName <- lexeme lastNamePart
  sep
  initial <- initialsBlock
  sep
  firstName  <- PersonFirstName . decap <$> lexeme namePart P.<?> "first name"
  sep
  nickname1 <- P.optionMaybe nicknamePart
  suffix <- suffixPart
  nickname2 <- P.optionMaybe nicknamePart
  randomNumberPart
  P.eof
  return (firstName, initial, lastName, suffix, nickname1 <|> nickname2)

properInitialedName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
properInitialedName = do
  lastName <- lexeme lastNamePart
  sep
  firstInitial <- PersonFirstName . (: []) <$> lexeme initialPart P.<?> "first initial"
  sep
  initial <- P.optionMaybe initialsBlock
  sep
  nickname <- P.optionMaybe nicknamePart
  suffix <- suffixPart
  randomNumberPart
  P.eof
  return (firstInitial, join initial, lastName, suffix, nickname)

initialedName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
initialedName = do
  lastName <- lexeme lastNamePart
  sep
  (firstInitial : initials) <- (P.many1 . P.try . lexeme $ initialPart) P.<?> "initial"
  sep
  nickname <- P.optionMaybe nicknamePart
  suffix <- suffixPart
  randomNumberPart
  P.eof
  let initials' = if null initials then Nothing else Just (PersonInitial initials)
  return (PersonFirstName [firstInitial], initials', lastName, suffix, nickname)

parseName :: PS (PersonFirstName, Maybe PersonInitial, PersonLastName, Maybe PersonNameSuffix, Maybe PersonNickname)
parseName = P.try fullName
        <|> P.try basicName
        <|> P.try initialedBasicName
        <|> P.try initialedBasicNameReverse
        <|> P.try properInitialedName
        <|> P.try initialedName
        <|> lastNameOnly
        P.<?> "name"

--------------------------------------------------------------------------------

parseUnit :: Parser String (S.Set (R Unit))
parseUnit = do
  unitNames <- map (T.unpack . T.strip) . T.split (`elem` ";,/") . T.pack <$> get
  return $ S.fromList ((addSource . mkUnit') `map` filter (not . null) unitNames)
  where
  addSource (Left u) = Left $ u { uSource = S.singleton dataSource }
  addSource _ = error "Right unit in addSource"
  mkUnit' :: String -> R Unit
  mkUnit' name | "Headquarters (" `isPrefixOf` name = mkUnit (Just Headquarters) name
               | "Detachment" `isInfixOf` name = mkUnit (Just Detachment) name
               | "Command" `isInfixOf` name = mkUnit (Just Command) name
               | "Division" `isInfixOf` name = mkUnit (Just Division) name
               | "Wing" `isInfixOf` name = mkUnit (Just Wing) name
               | "Brigade" `isInfixOf` name = mkUnit (Just Brigade) name
               | "Regiment" `isInfixOf` name = mkUnit (Just Regiment) name
               | "Group" `isInfixOf` name = mkUnit (Just Group) name
               | "Squadron" `isInfixOf` name = mkUnit (Just Squadron) name
               | "Battalion" `isInfixOf` name = mkUnit (Just Battalion) name
               | "Company" `isInfixOf` name = mkUnit (Just Company) name
               | "Section" `isInfixOf` name = mkUnit (Just Section) name
               | "Platoon" `isInfixOf` name = mkUnit (Just Platoon) name
               | otherwise = mkUnit Nothing name

--------------------------------------------------------------------------------

parseDate :: Parser String Day
parseDate = get >>= maybe err return . parseTime defaultTimeLocale "%d/%m/%Y"
  where err = tellFail "Date parse failed"

parseBorn :: Person -> Parser Record (Event Person)
parseBorn person = do
  date <- option $ parseDate . nonEmpty . arr trim . col "DOB" <??> "date of birth"
  place <- option $ locationName <$> nonEmpty . arr trim . col "Place of Birth" <??> "place of birth"
  if isNothing date && isNothing place
    then tellFail "Birth event parse failed"
    else return $ Event False "Born" date Nothing place (Left person)

parseDied :: Person -> Parser Record (Event Person)
parseDied person =
  Event <$> pure False
        <*> pure "Died"
        <*> (Just <$> parseDate . nonEmpty . arr trim . col "Date of Death")
        <*> pure Nothing
        <*> pure Nothing
        <*> pure (Left person)
        <??> "died event"

parseBuried :: Person -> Parser Record (Event Person)
parseBuried person =
  Event <$> pure True
        <*> pure "Buried"
        <*> pure Nothing
        <*> pure Nothing
        <*> (Just . locationName <$> nonEmpty . arr trim . col "Place of Burial")
        <*> pure (Left person)
        <??> "buried event"

--------------------------------------------------------------------------------

parseRecord :: Parser Record (Person, [Event Person])
parseRecord = do
  (firstName, initial, lastName, suffix, nickname) <- try (parsec parseName . nonEmpty . arr trim . col "Veteran's name") <??> "name"
  biog <- catMaybes <$> mapM (\x -> option $ try nonEmpty . arr trim . col x) ["Experiences", "Awards", "Scores"] <??> "biog"
  let biog' = if null biog
              then Nothing
              else Just . PersonBiography $ intercalate "\n\n" biog
  person <- Person <$> try (mkServiceNumber <$> option (nonEmpty . arr trim . col "Service Number") <??> "Service number")
                   <*> option (rankLookup . dropQ =<< try nonEmpty . arr trim . col "Rank") <??> "rank"
                   <*> pure firstName
                   <*> pure initial
                   <*> pure lastName
                   <*> pure suffix
                   <*> pure nickname
                   <*> role <??> "role"
                   <*> units <??> "units"
                   <*> pure (S.singleton $ Left $ Airforce "8th Air Force")
                   <*> pure S.empty
                   <*> pure biog'
                   <*> (base <|> pure S.empty) <??> "base"
                   <*> sources <??> "sources"
  events <- catMaybes <$> mapM (option . try . ($ person)) [parseBorn, parseDied, parseBuried]
  return (person, events)
  where
  dropQ :: String -> String
  dropQ ('?':xs) = xs
  dropQ xs = xs
  base = do
    baseNumbers <- map (T.unpack . T.strip) . T.split (`elem` ";,/") . T.pack <$> nonEmpty . arr trim . col "AAF Number"
    return $ S.fromList (mkPlace' `map` filter (not . null) baseNumbers)
  mkPlace' num = Left $ Place (Just $ StationNumber num) (PlaceName "") S.empty Nothing Nothing Nothing S.empty S.empty
  role = S.fromList <$> tryAll [ PersonRole <$> nonEmpty . arr trim . col "Position/Specialism" ]
  units = (parseUnit . try nonEmpty . arr trim . col "Group; Squadron") <??> "unit"
  sources = S.insert dataSource . S.fromList <$> tryAll
    [ SourceText <$> nonEmpty . arr trim . col "Record Source"
    , SourceText <$> nonEmpty . arr trim . col "Connective Phrase" ]

dataSource :: SourceMsg
dataSource = SourceText "Drawn from the records of the <a href=\"http://mightyeighth.org\">National Museum of the Mighty Eighth Air Force</a>, Savannah, Georgia"

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], ([ParseItem Person], [ParseItem (Event Person)]))
parse ctx filename csv = second (map (fmap fst) &&& concatMap (sequence . fmap snd)) $ parseCSV ctx parseRecord filename csv

parseRanks :: Context -> String -> CSV -> ([String], [ParseItem String])
parseRanks = flip parseCSV (try nonEmpty . arr trim . col "Rank")
