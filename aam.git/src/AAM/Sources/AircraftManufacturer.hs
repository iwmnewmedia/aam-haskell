module AAM.Sources.AircraftManufacturer where

import qualified Data.Map as M

import AAM.Entities.Common
import AAM.Entities.Aircraft

manufacturers :: M.Map AircraftManufacturer (ID AircraftManufacturer)
manufacturers = M.fromList $ concat
  [ mkManu 1  ["Bell"]
  , mkManu 2  ["Boeing"]
  , mkManu 3  ["Cessna"]
  , mkManu 4  ["Commonwealth"]
  , mkManu 5  ["Consolidated"]
  , mkManu 6  ["Curtiss"]
  , mkManu 7  ["Douglas"]
  , mkManu 8  ["Douglas-Long Beach"]
  , mkManu 9  ["Douglas-Tulsa"]
  , mkManu 10 ["Ford"]
  , mkManu 11 ["G & A"]
  , mkManu 12 ["General Aircraft"]
  , mkManu 13 ["Gibson Refrigerator"]
  , mkManu 14 ["Lockheed"]
  , mkManu 15 ["Lockheed-Vega"]
  , mkManu 16 ["Martin"]
  , mkManu 17 ["North American"]
  , mkManu 18 ["Northrop"]
  , mkManu 19 ["Northwestern"]
  , mkManu 20 ["Pratt-Read"]
  , mkManu 21 ["Republic"]
  , mkManu 22 ["Ridgefield"]
  , mkManu 23 ["Timm"]
  , mkManu 24 ["Waco"]
  , mkManu 25 ["Supermarine"]
  , mkManu 26 ["Avro"]
  , mkManu 27 ["Airspeed"]
  , mkManu 28 ["Beech"]
  , mkManu 29 ["Bristol"]
  , mkManu 30 ["De Havilland"]
  , mkManu 31 ["Miles"]
  , mkManu 32 ["Percival"]
  , mkManu 33 ["Westland"]
  , mkManu 34 ["Boulton Paul Aircraft"]
  , mkManu 35 ["Victory Aircraft"]
  , mkManu 36 ["Armstrong-Whitworth"]
  , mkManu 37 ["Hawker"]
  ]
  where
  mkManu n xs = (\x -> (AircraftManufacturer x, ID n)) `map` xs
