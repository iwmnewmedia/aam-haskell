module AAM.Sources.Rank where

import qualified Data.Map as M

import AAM.Entities.Common
import AAM.Entities.Rank

ranks :: M.Map Rank (ID Rank)
ranks = M.fromList $ concat
  [ mkRank (-1) ["Civilian"]
  , mkRank 1  ["Aircraftman"]
  , mkRank 2  ["Aircraftman Second Class", "Aircraftman Second Class (RAF)"]
  , mkRank 3  ["Leading Aircraftman"]
  , mkRank 4  ["Corporal"]
  , mkRank 5  ["Sergeant", "Sergeant (RAF)"]
  , mkRank 6  ["Flight Sergeant"]
  , mkRank 7  ["Warrant Officer"]
  , mkRank 8  ["Pilot Officer"]
  , mkRank 9  ["Flying Officer"]
  , mkRank 10 ["Flight Lieutenant"]
  , mkRank 11 ["Squadron Leader"]
  , mkRank 12 ["Wing Commander"]
  , mkRank 13 ["Group Captain"]
  , mkRank 14 ["Air Commodore"]
  , mkRank 15 ["Air Vice-Marshal"]
  , mkRank 16 ["Air Marshal"]
  , mkRank 17 ["Air Chief Marshal"]
  , mkRank 18 ["Marshal of the RAF"]
  , mkRank 19 ["Aviation Cadet"]
  , mkRank 20 ["Private", "Pvt", "private"]
  , mkRank 21 ["Private First Class", "PFC", "Private First Class (055)"]
  , mkRank 22 ["Private First Class (Specialist 4)"]
  , mkRank 23 ["Private First Class (Specialist 5)"]
  , mkRank 24 ["Airman"]
  , mkRank 25 ["Airman Third Class"]
  , mkRank 26 ["Airman Second Class"]
  , mkRank 27 ["Airman First Class"]
  , mkRank 28 ["Corporal"]
  , mkRank 29 ["Corporal (Technician Fifth Grade)"]
  , mkRank 30 ["Senior Airman"]
  , mkRank 31 ["Sergeant"]
  , mkRank 32 ["Sergeant (Technician Fourth Grade)"]
  , mkRank 33 ["Staff Sergeant", "S/Sergeant"]
  , mkRank 34 ["Staff Sergeant (Technician Third Grade)"]
  , mkRank 35 ["Technical Sergeant", "T/Sergeant", "T/ Sergeant"]
  , mkRank 36 ["Master Sergeant", "M/Sergeant"]
  , mkRank 37 ["Chief Master Sergeant"]
  , mkRank 38 ["First Sergeant", "First Sergeant (Police)"]
  , mkRank 39 ["Sergeant First Class"]
  , mkRank 40 ["Senior Master Sergeant"]
  , mkRank 41 ["Sergeant Major"]
  , mkRank 42 ["Command Sergeant Major"]
  , mkRank 43 ["Flight Officer", "Flight officer"]
  , mkRank 44 ["Warrant Officer"]
  , mkRank 45 ["Warrant Officer (Junior Grade)"]
  , mkRank 46 ["Warrant Officer (Chief)", "CWO"]
  , mkRank 47 ["Lieutenant"]
  , mkRank 48 ["Second Lieutenant"]
  , mkRank 49 ["First Lieutenant"]
  , mkRank 50 ["Captain", "captain"]
  , mkRank 51 ["Major", "MAJOR"]
  , mkRank 52 ["Lieutenant Colonel", "Lieutenant Col"]
  , mkRank 53 ["Colonel"]
  , mkRank 54 ["Brigadier General", "BG"]
  , mkRank 55 ["Major General"]
  , mkRank 56 ["Lieutenant General"]
  , mkRank 57 ["General"]
  ]
  where
  mkRank n xs = (\x -> (Rank x, ID n)) `map` xs
