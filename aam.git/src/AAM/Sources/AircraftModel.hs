module AAM.Sources.AircraftModel (models, exportModels) where

import Data.Bifunctor (second)
import Data.Tuple (swap)
import qualified Data.Map as M

import AAM.Entities.Common
import AAM.Entities.Aircraft

modelDefs :: [(ID AircraftModel, [String])]
modelDefs =
  [ (ID 1 , ["A-20 Havoc", "A-20", "DB-7"])
  , (ID 2 , ["A-26 Invader", "A-26"])
  , (ID 3 , ["A-31 Vengeance"])
  , (ID 4 , ["Lancaster", "Lancaster"])
  , (ID 5 , ["B-17 Flying Fortress", "B-17", "B-17s"])
  , (ID 6 , ["B-24 Liberator", "B-24", "B-24s"])
  , (ID 7 , ["B-25 Mitchell", "B-25"])
  , (ID 8 , ["B-26 Marauder", "B-26"])
  , (ID 9 , ["Hudson"])
  , (ID 10, ["Beaufighter", "BEAUFIGHTER"])
  , (ID 11, ["Hurricane"])
  , (ID 12, ["P-38 Lightning", "P-38"])
  , (ID 13, ["P-39 Airacobra", "P-39"])
  , (ID 14, ["P-40 Warhawk", "P-40"])
  , (ID 15, ["P-47 Thunderbolt", "P-47"])
  , (ID 16, ["P-51 Mustang", "P-51"])
  , (ID 17, ["P-61 Black Widow", "P-61"])
  , (ID 18, ["P-80 Shooting Star", "P-80", "F-80"])
  , (ID 19, ["Spitfire", "SPITFIRE", "Spitfire Mark V"])
  , (ID 20, ["Oxford"])
  , (ID 21, ["Anson", "AVERO ANSON"])
  , (ID 22, ["18 Expeditor"])
  , (ID 23, ["Dominie"])
  , (ID 24, ["L-1 Vigilant"])
  , (ID 25, ["L-4 Grasshopper"])
  , (ID 26, ["L-5 Sentinel", "L-5"])
  , (ID 27, ["Master"])
  , (ID 28, ["OA-10 Catalina", "OA-10"])
  , (ID 29, ["Proctor", "PROCTOR III"])
  , (ID 30, ["Lysander"])
  , (ID 31, ["F-3/3A Havoc"])
  , (ID 32, ["F-4 Lightning"])
  , (ID 33, ["F-5 Lightning", "F-5"])
  , (ID 34, ["F-6 Mustang"])
  , (ID 35, ["Mosquito", "MOSQUITO"])
  , (ID 36, ["C-46 Commando", "C-46"])
  , (ID 37, ["C-47 Skytrain", "C-47"])
  , (ID 38, ["C-53 Skytrooper", "C-53"])
  , (ID 39, ["UC-64/64A Norseman"])
  , (ID 40, ["C-56 Lodestar"])
  , (ID 41, ["UC-61 Forwarder"])
  , (ID 42, ["CG-4A Haig", "CG-4"])
  , (ID 43, ["CG-13A"])
  , (ID 44, ["Horsa", "HORSA", "GLIDER"])
  , (ID 45, ["AT-6 Texan/Harvard"])
  , (ID 46, ["AT-17 Bobcat", "UC-78"])
  , (ID 47, ["Tiger Moth"])
  , (ID 48, ["Defiant"])
  ]

models :: M.Map AircraftModel (ID AircraftModel)
models = M.fromList $ mkModel `concatMap` modelDefs
  where
  mkModel (aId, xs) = (\x -> (AircraftModel x, aId)) `map` xs

exportModels :: M.Map AircraftModel (ID AircraftModel)
exportModels = M.fromList $ (swap . second (AircraftModel . head)) `map` modelDefs
