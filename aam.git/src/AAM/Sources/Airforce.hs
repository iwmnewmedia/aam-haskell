module AAM.Sources.Airforce (knownAirforces) where

import AAM.Entities.Airforce

knownAirforces :: [Airforce]
knownAirforces = map Airforce
  [ "8th Air Force"
  , "9th Air Force"
  , "12th Air Force"
  , "15th Air Force"
  , "14th Air Force"
  , "1st Air Force" ]
