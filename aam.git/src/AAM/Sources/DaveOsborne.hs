module AAM.Sources.DaveOsborne (parse) where

import Prelude hiding ((.))

import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Arrow (arr)
import Control.Applicative
import Control.Category
import Text.CSV (CSV, Record)

import qualified Data.Set as S

--------------------------------------------------------------------------------

parseRecord :: Parser Record Aircraft
parseRecord = Aircraft
  <$> (AircraftSerial <$> try nonEmpty . arr trim . col "Number" <??> "Number")
  <*> pure (Just . Left $ AircraftModel "B-17")
  <*> pure Nothing
  <*> pure Nothing
  <*> option (AircraftDescription <$> try nonEmpty . arr trim . col "Free text" <??> "Description text")
  <*> pure (S.singleton . Left $ Airforce "8th Air Force")
  <*> pure S.empty
  <*> pure (S.singleton $ SourceText "Dave Osborne, B-17 Fortress Master Log")

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], [ParseItem Aircraft])
parse ctx = parseCSV ctx parseRecord
