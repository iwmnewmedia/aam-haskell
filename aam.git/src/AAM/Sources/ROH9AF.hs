module AAM.Sources.ROH9AF (parse) where

import Prelude hiding ((.), id)
import AAM.Context
import AAM.Entities
import AAM.Parser
import Control.Arrow (arr)
import Control.Applicative
import Control.Category
import Data.Either (lefts)
import Data.Maybe (catMaybes)
import Data.Time (Day, parseTime)
import Text.CSV (CSV, Record)
import System.Locale (defaultTimeLocale)
import qualified Data.Set as S
import qualified Text.Parsec as P

--------------------------------------------------------------------------------

parseFirstName :: PS (PersonFirstName, Maybe PersonInitial)
parseFirstName = do
  firstName <- PersonFirstName <$> lexeme (P.many1 $ P.noneOf " ") P.<?> "first name"
  initial <- P.optionMaybe (PersonInitial . return <$> lexeme P.upper) P.<?> "initial"
  return (firstName, initial)

--------------------------------------------------------------------------------

parseLastName :: PS (PersonLastName, Maybe PersonNameSuffix)
parseLastName = do
  lastName <- PersonLastName <$> lexeme (P.many1 $ P.noneOf " ") P.<?> "last name"
  suffix <- P.optionMaybe (PersonNameSuffix <$> lexeme properWord) P.<?> "name suffix"
  return (lastName, suffix)

------------------------------------------------------------------------------

parseDate :: Parser String (Maybe Day)
parseDate = arr $ parseTime defaultTimeLocale "%e-%b-%Y"

------------------------------------------------------------------------------

parsePersonEvent :: Parser Record (R Person -> Event Person)
parsePersonEvent = Event
  <$> pure False
  <*> nonEmpty . arr trim . col "Fate"
  <*> pure Nothing
  <*> parseDate . col "Date"
  <*> pure Nothing

------------------------------------------------------------------------------

ninthAF :: S.Set (R Airforce)
ninthAF = S.singleton $ Left $ Airforce "9th Air Force"

parsePerson :: Parser Record Person
parsePerson = do
  serviceNum <- try $ mkServiceNumber <$> option (arr trim . nonEmpty . col "Serial number") <??> "Service number"
  (firstName, initial) <- (parsec parseFirstName . nonEmpty . col "First name" <|> pure (PersonFirstName "", Nothing))  <??> "first name & initial"
  (lastName, suffix) <- parsec parseLastName . nonEmpty . col "Last name"  <??> "last name & suffix"
  rank <- parseRank
  unit <- S.fromList <$> tryAll [ mkUnit (Just Squadron) <$> nonEmpty . arr trim . col "Squadron"
                                , mkUnit (Just Group) <$> nonEmpty . arr trim . col "Group"
                                ] <??> "unit"
  src <- S.insert dataSource <$> source <??> "source"
  return $ Person serviceNum rank firstName initial lastName suffix Nothing S.empty unit ninthAF S.empty Nothing S.empty src
  where
  source = S.fromList . map SourceText <$> tryAll [ nonEmpty . arr trim . col "Source" ]
  dataSource = SourceText "9th Air Force Roll of Honor (IWM in liaison with Ninth Air Force veterans and group associations, American Battle Monuments Commission, and Buddies of the Ninth Association)"

parseRank :: Parser Record (Maybe (R Rank))
parseRank = do
  rank <- try . option $ nonEmpty . arr trim . col "Rank" <??> "rank"
  maybe (return Nothing) (\r -> Just <$> rankLookup r) rank

parseRecord :: Parser Record (Person, Maybe (Event Person))
parseRecord = do
  person <- parsePerson <??> "person"
  personEvent <- option $ try parsePersonEvent <??> "person event"
  let personEvent' = ($ Left person) <$> personEvent
  return (person, personEvent')

--------------------------------------------------------------------------------

parse :: Context -> String -> CSV -> ([String], ([ParseItem Person],
                                      [ParseItem (Event Person)],
                                     [ParseItem Unit]))
parse ctx filename csv = (errors, (ps, catMaybes pes, extractUnits ps))
  where
  (errors, rows) = parseCSV ctx parseRecord filename csv
  (ps, pes) = unzip (rewrap `map` rows)
  rewrap (ParseItem r (p, pe)) = (ParseItem r p, ParseItem r <$> pe)
  extractUnits = concatMap $ \(ParseItem src p) -> ParseItem src `map` lefts (S.toList (pUnit p))
