module AAM.Context where

import AAM.Entities
import Text.CSV
import qualified Data.Map as M

data Context = Context
  { headers :: Record
  , ranks :: M.Map String (ID Rank)
  }

nullCtx :: Context
nullCtx = Context { headers = [], ranks = M.empty }
